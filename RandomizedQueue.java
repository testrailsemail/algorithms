import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

public class RandomizedQueue<Item> implements Iterable<Item> {
  private Item[] q;       // queue elements
  private int n;          // number of elements on queue
//  private int first;      // index of first element of queue
  private int last;       // index of next available slot
  
  public RandomizedQueue() {                 // construct an empty randomized queue
    n = 0;
//    first = 0;
    last = 0;
    q = (Item[]) new Object[2];
  }
  
  public boolean isEmpty() {                 // is the queue empty?
    return n == 0;
  }
  
  public int size() {                        // return the number of items on the queue
    return n;
  }
  
  private void resize(int capacity) {
    assert capacity >= n;
    Item[] temp = (Item[]) new Object[capacity];
    for (int i = 0; i < n; i++) {
//        temp[i] = q[(first + i) % q.length];
      temp[i] = q[(i) % q.length];
    }
    q = temp;
//    first = 0;
    last  = n;
  }
  
  public void enqueue(Item item) {           // add the item
    if (item == null) throw new NullPointerException();
    if (n == q.length) resize(2*n);
    q[last++] = item;
    n++;
//    if (last == q.length) last = 0;
  }
  
  public Item dequeue() {                    // remove and return a random item
    if (isEmpty()) throw new NoSuchElementException();
    int i = StdRandom.uniform(n);
    Item temp = q[i];
    n--;
    if (i == last - 1) {
      q[i] = null;
      last--;
    }
    else {
      q[i] = q[last-1];
      q[last-1] = null;
      last--;
    }
    if (n > 0 && n == q.length/4) resize(q.length/2);
    
//    Item temp = q[first];
    return temp;
    
  }
    
  public Item sample() {                     // return (but do not remove) a random item
    if (isEmpty()) throw new NoSuchElementException();
    int i = StdRandom.uniform(n);
    return q[i];
  }
  
  private int len() { return q.length; }
  
  public String toString() {
    StringBuilder s = new StringBuilder();
    for (Item item : this.q) {
        s.append(item);
        s.append(' ');
    }
    return s.toString();
  }
  
  public Iterator<Item> iterator() {         // return an independent iterator over items in random order
    return new ListIterator<Item>();
  }
  
  private class ListIterator<Item> implements Iterator<Item> {
    private int current;
    private Item[] t;       // queue elements
//    private int end;
//    private int m;
    
    public ListIterator() {
//        current = first;
      current = 0;
//      t = (Item[]) q;
      t = (Item[]) new Object[n];
      System.arraycopy(q, 0, t, 0, n);
      StdRandom.shuffle(t);
//      m = n;
//      end = 0;
    }

    public boolean hasNext() {
        return current < n;   // end < m
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }

    public Item next() {
        if (!hasNext()) throw new NoSuchElementException();
        Item temp = t[current];
//        iterating randomly through queue not randomly dequeued 
//        if (temp == null) {
//          current++;
//          return next();
//        }
//        current = (current + 1) % q.length;
//        end++;
        current++;
        return temp;
    }
  }
  
  public static void main(String[] args) {   // unit testing (optional)
    RandomizedQueue<String> queue = new RandomizedQueue<String>();
    queue.enqueue("a");
    StdOut.println("(" + queue.sample() + " is in queue)");
    StdOut.println("removed:   " + queue.dequeue());
    queue.enqueue("a");
    queue.enqueue("b");
    queue.enqueue("c");
    queue.enqueue("d");
    for (String i : queue) {
      StdOut.println("element:      " + i);
    }
    StdOut.println("removed:   " + queue.dequeue());
    StdOut.println("removed:   " + queue.dequeue());
    StdOut.println("removed:   " + queue.dequeue());
//    StdOut.println("array:          " + queue.q);
    queue.enqueue("1");
    queue.enqueue("2");
    queue.enqueue("3");
    queue.enqueue("8");
    StdOut.println("removed:   " + queue.dequeue());
    StdOut.println("removed:   " + queue.dequeue());
    StdOut.println("removed:   " + queue.dequeue());
    StdOut.println("removed:   " + queue.dequeue());
    StdOut.println("num elements:   " + queue.n);
    StdOut.println("elements:       " + queue);
    StdOut.print("elements:       ");
    for (String i : queue) {
      StdOut.print(i + " ");
    }
    StdOut.println();
//    String[] c = (String[]) queue.q;
//    int l = c.length;
    StdOut.println("array size:     " + queue.len());
  }
}