require 'set'
require_relative "digraph"
require_relative "sap"
require_relative 'DirectedCycle'

# // http://coursera.cs.princeton.edu/algs4/assignments/wordnet.html
# // http://coursera.cs.princeton.edu/algs4/checklists/wordnet.html
class WordNet
  # private final Map<String, Set<Integer>> :nounToId
  # private final Map<Integer, String> :idsToSynset
  # private final SAP :sap
  # // constructor takes the name of the two input files
  def initialize(synsets, hypernyms)
    raise ArgumentError.new("Two files are needed: synsets and hypernyms") if (synsets == nil || hypernyms == nil)
    @idsToSynset = {}
    @nounToId = {}
    initSynSet(synsets)
    hnyms = initHypernyms(hypernyms)
    cycle = DirectedCycle.new(hnyms)
    raise ArgumentError.new("The input does not correspond to a rooted DAG!") if (cycle.hasCycle? || !isRootedDAG?(hnyms))
    @sap =  SAP.new(hnyms);
  end

  private
  def initSynSet(synSets)
    synset = File.new(synSets.to_s)

    until synset.eof?
      parts = synset.readline.split(",")
      id = parts[0].to_i
      syns = parts[1]
      @idsToSynset[id] = syns
# //    part[2] is a word definition
      syns.split(" ").each do |word|
        ids = @nounToId[word]
        ids = Set.new if ids == nil
        ids.add(id);
        @nounToId[word] = ids
      end
    end
  end

  def initHypernyms(hypernyms)
    g = Digraph.new(@idsToSynset.size)

    nyms = File.new(hypernyms.to_s)
    until nyms.eof?
      parts = nyms.readline.split(",")
      id = parts[0].to_i
      (1...parts.length).each do |i|
        part = parts[i].to_i
        g.addEdge(id, part)
      end
    end
    puts "vertices = #{g.v}, edges = #{g.e}"

    g
  end

  def isRootedDAG?(g)
    roots = 0
    (0...g.v).each do |i|
      roots += 1 if g.outdegree(i) == 0
      return false if (roots > 1)
    end
    roots == 1
  end

  # returns all WordNet nouns
  public
  def nouns
    @nounToId.keys
  end

  # is the word a WordNet noun?
  def isNoun?(word)
    raise ArgumentError.new if word == nil
    return false unless word.is_a? String
    @nounToId.key? word
  end

  # distance between nounA and nounB (defined below)
  def distance(nounA, nounB)
    raise ArgumentError.new("Words must be nouns") if (!isNoun?(nounA) || !isNoun?(nounB))
    idsA = @nounToId[nounA]
    idsB = @nounToId[nounB]

    @sap.lengths(idsA, idsB)
  end

   # a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
   # in a shortest ancestral path (defined below)
  def sap(nounA, nounB)
    raise ArgumentError.new("Words must be nouns") if (!isNoun?(nounA) || !isNoun?(nounB))
    idsA = @nounToId[nounA]
    idsB = @nounToId[nounB]
    commonAncestor = @sap.ancestors(idsA, idsB)

    @idsToSynset[commonAncestor]
  end

  # do unit testing of this class
  def self.main(args)
    wordNet =  WordNet.new(args[0], args[1])

    until (s = STDIN.gets.chomp) == ''
      v = s.to_s
      w = STDIN.gets.chomp
      unless wordNet.isNoun?(v)
        puts "#{v} is not in the word net"
        next
      end
      unless wordNet.isNoun?(w)
        puts "#{w} is not in the word net"
        next
      end

      length = wordNet.distance(v, w)
      ancestor = wordNet.sap(v, w)
      puts "length = #{length}, ancestor = #{ancestor}"
    end
  end
end

# WordNet.main(ARGV)
