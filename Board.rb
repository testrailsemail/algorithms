class Board
  attr_reader :tiles
  def initialize(blocks)                          # construct a board from an n-by-n array of blocks
    
    raise ArgumentError.new( "You gave me a nil!") if blocks.nil?

    @n = blocks.length
    @tiles = []
    
    blocks.each do |arr|
      @tiles << arr.clone
      raise ArgumentError.new("one of the block is repeated") if arr.detect{ |e| arr.count(e) > 1 }
      # raise ArgumentError.new("wrong size") if arr.length != @n
    end
  end
                                         
  def dimension                 # board dimension n
    @n
  end
  
  def hamming                    # number of blocks out of place
    count = 0
    @tiles.each_with_index { |tile, i|
      tile.each_with_index { |e, j|
        count += 1 if (e != 0 && e != 1 + @n*i+j) }
    }
    count  
  end
  
  def manhattan                # sum of Manhattan distances between blocks and goal
    count = 0
    @tiles.each_with_index { |tile, i|
      tile.each_with_index { |e, j|
        count += ((e - 1)/@n - i).abs + ((e - 1) % @n - j).abs if (e != 0) }
    }
    count      
  end
  
  def isGoal?               # is this board the goal board?
    goal = []
    (0...@n).each do |i|
      goal[i] ||= []
      (0...@n).each { |j|
        goal[i][j] = 1 + @n*i+j }
    end
    goal[@n-1][@n-1] = 0
    @tiles == goal
  end
  
  def twin                    # a board that is obtained by exchanging any pair of blocks
    arr = []
    pointers = []
    flag = 0
    @tiles.each { |ar|
      arr << ar.clone }
    prng = Random.new
    row = prng.rand(@n)
    while (flag != 1) do
      (0..3).each {|i| pointers[i] = prng.rand(@n) }
      
      p = (pointers[0] == pointers[2]) && (pointers[1] == pointers[3])
      if (!p && arr[pointers[0]][pointers[1]] != 0 && arr[pointers[2]][pointers[3]] != 0)
        arr[pointers[0]][pointers[1]], arr[pointers[2]][pointers[3]] = arr[pointers[2]][pointers[3]], arr[pointers[0]][pointers[1]]
        break
      end
    end
    twin = Board.new(arr)  
  end
  
  def exch(a, i, j, k, l)
    b = []
    a.each do |ar|
      b << ar.clone
    end
    b[i][j], b[k][l] = b[k][l], b[i][j]
    b
  end
  
  def equals?(other)      # does this board equal y?
    return true if (other == self) 
    return false if (other == nil) 
    return false if (other.class != self.class) 
    @tiles == other.tiles
  end
  
  def neighbors    # all neighboring boards
    neighbors = []
    row = @tiles.detect{|aa| aa.include?(0)}
    col = row.index(0)
    row = @tiles.index(row)
    for y in [0, col - 1].max..[col + 1, @n-1].min do
      arr = exch(@tiles, row, col, row, y) if (y != col)
      neighbors.push(Board.new(arr)) if (y != col)
    end

    for x in [0, row-1].max..[row+1, @n-1].min do
      arr = exch(@tiles, row, col, x, col) if (x != row)
      neighbors.push(Board.new(arr)) if (x != row)
    end
    neighbors
  end
  
  def to_s              # string representation of this board (in the output format specified below)
    str = ""
    str << "#{@n}\n"
    @tiles.each { |row| row.each { |e| str << ("%2s " % e) }; str << "\n" }# row.join(" "); str << "\n"}
    
    str
  end

  def self.test(*args)  # unit tests (not graded)
    arr = []
    n = 3
    (0...n).each do |i|
      arr[i] ||= []
      (0...n).each { |j|
        arr[i][j] = 1 + n*i+j }
    end
    arr[n-1][n-1] = 0
    bo = Board.new(arr)
    puts "equality check: #{bo.isGoal?}"
    puts "initial board:  #{bo}"
    for neighbor in bo.neighbors do
      puts "neighbor board:  #{neighbor}"
    end
    puts "twin board:  #{bo.twin}"
    puts "initial board:  #{bo}"
    bo = bo.twin
    puts "twin board:  #{bo}"
    puts "boardtwin manh #{bo.manhattan}"
    puts "boardtwin ham #{bo.hamming}"
    bo = bo.twin
    puts "twintwintwin board:  #{bo}"
    puts "boardtwintwin manh #{bo.manhattan}"
    puts "boardtwintwin ham #{bo.hamming}"
    
  end
end

class Element
  include Comparable

  attr_accessor :name, :priority

  def initialize(name, priority)
    @name, @priority = name, priority
  end

  def <=>(other)
    @priority <=> other.priority
  end
end

class MinPQ
  attr_reader :elements
  def initialize
    @elements = [nil]
  end

  def <<(element)
    @elements << element
    bubble_up(@elements.size - 1)
  end

  def exchange(source, target)
    @elements[source], @elements[target] = @elements[target], @elements[source]
  end

  def bubble_up(indeks)
    parent_index = indeks/2

    # return if we reach the root element
    return if indeks <= 1

    # or if the parent is already greater than the child
    return if @elements[parent_index] <= @elements[indeks]

    # otherwise we exchange the child with the parent
    exchange(indeks, parent_index)

    # and keep bubbling up
    bubble_up(parent_index)
  end

  def pop
    # exchange the root with the last element
    exchange(1, @elements.size - 1)

    # remove the last element of the list
    max = @elements.pop

    # and make sure the tree is ordered again
    bubble_down(1)
    max
  end

  def bubble_down(index)
    child_index = (index * 2)

    # stop if we reach the bottom of the tree
    return if child_index > @elements.size - 1

    # make sure we get the largest child
    not_the_last_element = child_index < @elements.size - 1
    left_element = @elements[child_index]
    right_element = @elements[child_index + 1]
    child_index += 1 if not_the_last_element && right_element < left_element

    # there is no need to continue if the parent element is already bigger
    # then its children
    return if @elements[index] <= @elements[child_index]

    exchange(index, child_index)

    # repeat the process until we reach a point where the parent
    # is larger than its children
    bubble_down(child_index)
  end

end

class Solver
 
  class Node
    include Comparable
    attr_accessor :board, :moves, :previous, :priority
    
    def initialize(item, moves, previousNode = nil, priority = "manhattan")
      @board = item
      @moves = moves
      @previous = previousNode
      @priority = priority == "manhattan" ? priorityM : priorityH
    end

    def priorityM
      pri = moves + board.manhattan
    end
     
    def priorityH
      pri = moves + board.hamming
    end

    def <=>(other)
      @priority <=> other.priority
    end

  end
   
  def initialize(initial)           # find a solution to the initial board (using the A* algorithm)
    raise ArgumentError.new( "null initial puzzle board") if (initial.nil?) 
    
    @moves = 0
    @goalNode = nil
    initialNode =  Node.new(initial, moves)
    initialTwin =  Node.new(initial.twin, moves)

    pq =  MinPQ.new
    pqTwin =  MinPQ.new
      
    pq << initialNode
    pqTwin << initialTwin
      
    current = initialNode
    currentTwin = initialTwin
      
    initialSolved = false
    twinSolved = false
    
    movesTwin = 0

    while (!initialSolved && !twinSolved) do
      current = pq.pop
      if current.nil?
        @isSolvable = false
        @moves = -1
        break
      end
      @moves = current.moves
      currentTwin = pqTwin.pop
      movesTwin = currentTwin.moves
      if current.board.isGoal? 
        @isSolvable = true
        initialSolved = true
        @goalNode = current
        break
      end
      if currentTwin.board.isGoal? || twinSolved
        twinSolved = true
        @isSolvable = false
        @moves = -1
        break
      end

      @moves += 1
      movesTwin += 1

      
      current.board.neighbors.each do |n|
        temp = current
        flag = true
        while not temp.previous.nil? do
          if n.equals?(temp.previous.board)
            flag = false;
            break
          end
          temp = temp.previous
        end
        
        pq << Node.new(n, @moves, current) if flag
      end
      
      if !twinSolved 
        currentTwin.board.neighbors.each do |n|
          flag = true
          temp = currentTwin
          while not temp.previous.nil? do
            if n.equals?(temp.previous.board) 
              flag = false
              break
            end
            temp = temp.previous
          end
          pqTwin << Node.new(n, movesTwin, currentTwin) if flag
        end
      end

    end
  end  

    # private Comparator<Node> manhattan() {
    #   return new Comparator<Node>() {
    #     @Override
    #     public int compare(Node o1, Node o2) {
    #         return Integer.compare(o1.priorityM(), o2.priorityM());
    #     }
    #   };
    # }
    
    # private Comparator<Node> hamming() {
    #   return new Comparator<Node>() {
    #     @Override
    #     public int compare(Node o1, Node o2) {
    #         return Integer.compare(o1.priorityH(), o2.priorityH());
    #     }
    #   };
    # }
    
  def isSolvable?            # is the initial board solvable?
    @isSolvable
  end
    
  def moves                   # min number of moves to solve initial board; -1 if unsolvable
    @moves
  end
    
  def solution     # sequence of boards in a shortest solution; null if unsolvable
    return nil if not @isSolvable
    solution = []
    current = @goalNode
    while not current.nil? do
      solution.unshift(current.board)
      current = current.previous
    end
    solution
  end
    
  def self.test(*args) # solve a slider puzzle (given below)
    # create initial board from file
    # In in = new In(args[0]);
    # int n = in.readInt();
    # int[][] blocks = new int[n][n];
    # for (int i = 0; i < n; i++)
    #     for (int j = 0; j < n; j++)
    #         blocks[i][j] = in.readInt();
    # Board initial = new Board(blocks);
    # blocks = [[1, 0], [2, 3]]
    # blocks = [[8, 6, 7], [2, 5, 4], [1, 3, 0]]
    blocks = []
    n = gets.chomp.to_i
    # p n
    # blocks = [[8, 6, 7], [2, 5, 4], [3, 0, 1]]
    while (item = gets) do
      # break if item.chomp.empty?
      a = item.chomp.split(" ").map(&:to_i)
      blocks << a
    end
    # p blocks
    initial =  Board.new(blocks)

    # solve the puzzle
    solver =  Solver.new(initial)

    # print solution to standard output
    if not solver.isSolvable?
      puts "No solution possible"
    else 
        puts "Minimum number of moves =  #{solver.moves}"
        solver.solution.each {|board|  puts board }
    end
    nil
  end
end

# p ARGV
# ARGF.each_with_index do |line, idx|
#     print ARGF.filename, ": ", idx, "; ", line
#     puts
#     a = line.split
#     puts a
# end
Solver.test(ARGF)