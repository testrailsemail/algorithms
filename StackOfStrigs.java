
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

public class StackOfStrigs {
  private Node first = null;
  private int n;
  
  
  private class Node {
    private String item;
    private Node next;
  }
  
  public StackOfStrigs() {
    first = null;
    n = 0;
  }
  
  public void push(String item) {
    Node oldfirst = first;
    first = new Node();
    first.item = item;
    first.next = oldfirst;
    n++;
  }
  
  public String pop() {
    String item = first.item;
    first = first.next;
    n--;
    return item;
  }
  
  public boolean isEmpty() {
    return first == null;
  }
  
  public int size() {
    return n;
  }
  
  
  public static void main(String[] args) {
    int counta = 0;
    int countb = 0;
    int countc = 0;
    for (int k = 0; k < 10000; k++) {
      StackOfStrigs stack1 = new StackOfStrigs();
      StackOfStrigs stack2 = new StackOfStrigs();
      int ji = StdRandom.uniform(2);
      if (ji == 0) {
        stack1.push("a");
        stack1.push("b");
      }
      else {
        stack1.push("b");
        stack1.push("a");
      }
      int je = StdRandom.uniform(2);
      if (je == 0) {
        stack2.push(stack1.pop());
//        je = StdRandom.uniform(2);
//        if (je == 0) stack2.push(stack1.pop());
//        else stack2.push("c");
      }
      else {
        stack2.push("c");
//        stack2.push(stack1.pop());
//        stack2.push(stack1.pop());
      }
      
      String v = stack2.pop();
      if (v.equals("a")) counta++;
      else if (v.equals("b")) countb++;
      else countc++;
      
    }
    StdOut.print("a " + counta + " b " + countb + " c " + countc);
    
    
    
    
    
//    while (!StdIn.isEmpty()) {
//      String s = StdIn.readString();
//      if (s.equals("-")) StdOut.print(stack.pop());
//      else stack.push(s);
//    }

  }

}
