# Transformed for ruby by testa19 (ez)
# ******************************************************************************
# *  Data files:   https://algs4.cs.princeton.edu/24pq/tinyPQ.txt
# *
# *  Generic min priority queue implementation with a binary heap.
# *  Can be used with a comparator instead of the natural order.
# *
# *  % java MinPQ < tinyPQ.txt
# *  E A E (6 left on pq)
# *
# *  We use a one-based array to simplify parent and child calculations.
# *
# *  Can be optimized by replacing full exchanges with half exchanges
# *  (ala insertion sort).
# *
# ******************************************************************************
# **
# *  The {@code MinPQ} class represents a priority queue of generic keys.
# *  It supports the usual <em>insert</em> and <em>delete-the-minimum</em>
# *  operations, along with methods for peeking at the minimum key,
# *  testing if the priority queue is empty, and iterating through
# *  the keys.
# *  <p>
# *  This implementation uses a binary heap.
# *  The <em>insert</em> and <em>delete-the-minimum</em> operations take
# *  logarithmic amortized time.
# *  The <em>min</em>, <em>size</em>, and <em>is-empty</em> operations take constant time.
# *  Construction takes time proportional to the specified capacity or the number of
# *  items used to initialize the data structure.
# *  <p>
# *  For additional documentation, see <a href="https://algs4.cs.princeton.edu/24pq">Section 2.4</a> of
# *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
# *
# *  @author Robert Sedgewick
# *  @author Kevin Wayne
#

class MinPQ
  include Enumerable
  attr_accessor :pq
  # /**
  #  * Initializes an empty priority queue using the given comparator.
  #  */
  def initialize(comparator = nil) # ->(a,b) { a <=> b }
      @pq = [nil] # (Key[]) new Object[initCapacity + 1];
      # @n = 0
      @comparator = comparator
  end

  # /**
  #  * Initializes a priority queue from the array of keys.
  #  * <p>
  #  * Takes time proportional to the number of keys, using sink-based heap construction.
  #  *
  #  * @param   keys the array of keys
  #  */         comparator -  a lambda function for comparison
  #                           if keys do not include comparable module
  def self.from_ary(keys, comparator = nil)
    n = keys.length
    pqu = MinPQ.new(comparator)
    pqu.pq.concat(keys) #.clone
    (1..n/2).reverse_each { |k| pqu.send(:sink, k)}
    raise "This is wrong" unless pqu.send(:isMinHeap?)
    pqu
  end

  # Returns true if this priority queue is empty.
  def isEmpty?
    # @n == 0
    @pq.size - 1 == 0
  end

  #  * @return the number of keys on this priority queue
  #  */
  def size
    # @n
    @pq.size - 1
  end

  #  * @return a smallest key on this priority queue
  #  * @throws ArgumentError if this priority queue is empty
  #  */
  def min
    raise ArgumentError.new("Priority queue underflow") if isEmpty?
    @pq[1]
  end

  # /**
  #  * Adds a new key to this priority queue.
  #  *
  #  * @param  key to add to this priority queue
  #  */
  def insert(key)
    # // add x, and percolate it up to maintain heap invariant
    # @pq[@n+=1] = key
    @pq << key
    # swim(@n)
    swim(size)
    # raise "This is wrong" unless isMinHeap?
  end
  alias_method :<<, :insert

  # /**
  #  * Removes and returns a smallest key on this priority queue.
  #  *
  #  * @return a smallest key on this priority queue
  #  * @throws NoSuchElementException if this priority queue is empty
  #  */
  def delMin
    raise ArgumentError.new("Priority queue underflow") if isEmpty?
    # min = @pq[1]
    exch(1, size)
    min = @pq.pop
    # @n -= 1
    sink(1)
    # raise "This is wrong" unless isMinHeap?
    min
  end

  def each
    return enum_for(:each) unless block_given?
    iter = iterator

    while true
      begin
        vs = iter.next
      rescue StopIteration
        return $!.result
      end
      yield vs
    end
  end

# /***************************************************************************
#  * Helper functions to restore the heap invariant.
#  ***************************************************************************/

  private
  def swim(k)
    while (k > 1 && greater(k/2, k)) do
      exch(k, k/2);
      k = k/2;
    end
  end

  def sink(k)
    while (2*k <= size) do
      j = 2*k
      j += 1 if (j < size && greater(j, j+1))
      break if !greater(k, j)
      exch(k, j);
      k = j
    end
  end

# /***************************************************************************
#  * Helper functions for compares and swaps.
#  ***************************************************************************/
  def greater(i, j)
    return @pq[i] > @pq[j] if @comparator == nil
    @comparator[@pq[i], @pq[j]] > 0
  end

  def exch(i, j)
    @pq[i], @pq[j] = @pq[j], @pq[i]
  end

 # is pq[1..N] a min heap?
  def isMinHeap?
    minHeap?(1)
  end
  # is subtree of pq[1..n] rooted at k a min heap?
  def minHeap?(k)
    return true if (k > size)
    left = 2*k;
    right = 2*k + 1;
    return false if (left   <= size && greater(k, left))
    return false if (right  <= size && greater(k, right))
    return minHeap?(left) && minHeap?(right)
  end

  # /**
  #  * Returns an iterator that iterates over the keys on this priority queue
  #  * in ascending order.
  #  * <p>
  #  * The iterator doesn't implement {@code remove()} since it's optional.
  #  *
  #  * @return an iterator that iterates over the keys in ascending order
  #  */
  def iterator
    HeapIterator.new(@pq, @comparator)
  end

  class HeapIterator
    # include Enumerable
    # add all items to copy of heap
    # takes linear time since already in heap order so no keys move
    def initialize(pq, comparator = nil)
      @copy =  MinPQ.new(comparator)
      pq.drop(1).each { |i| @copy.insert(i) }
    end

    def hasNext?
      !@copy.isEmpty?
    end
    def remove
      raise "Unsupported operation"
    end

    def next
      raise StopIteration.new if !hasNext?
      @copy.delMin
    end
  end

  # /**
  #  * Unit tests the {@code MinPQ} data type.
  #  *
  #  * @param args the command-line arguments
  #  */
  def self.main(args)
    pq =  MinPQ.new
    unless ((inp = gets.chomp) == "q")

      item = inp
      item == "-" ? (puts "#{pq.delMin} " if !pq.isEmpty?) : pq.insert(item)
    end
    puts "(#{pq.size} left on pq)"
  end
end
