# -*- coding: utf-8 -*-
"""
Created on Wed Mar 29 13:09:40 2017

@author: admin
"""
import random
class WeightedQuickUnionUF(object):
    
    def __init__(self, n):
    
        try:
            int(n)
        except ValueError:
            print( "That was no valid number.  Try again...")
        self.count = n    # number of components
        self.size = [1]*n    # size[i] = number of sites in subtree rooted at i 
        self.parent = [i for i in range(n)] # parent[i] = parent of i
            
    def find(self, p):
        self.validate(p)
        while p != self.parent[p]:
            p = self.parent[p]
        return p

    def connected(self, p, q):
        return self.find(p) == self.find(q)

    def union(self, p, q):
        rootP = self.find(p)
        rootQ = self.find(q)
        if rootP == rootQ:
            return
        if self.size[rootP] < self.size[rootQ]:
            self.parent[rootP] = rootQ
            self.size[rootQ] += self.size[rootP]
        else:
            self.parent[rootQ] = rootP
            self.size[rootP] += self.size[rootQ]

        self.count -= 1
        
    def validate(self, p):
        n = len(self.parent)
        if p < 0 or p >= n:
            raise ValueError

    @classmethod    
    def test(cls, n):
        uf = 0
        uf = cls(n)
        nums = "1"
        while (nums != ""):
            print( "Two integers between 0 and {} or Enter key to abort".format(n-1))
            nums = input()
            try:
                p, q = nums.split()
            
                p = int(p)
                q = int(q)
            except ValueError:
                
                continue
            
            try:
                if uf.connected(p, q):
                    continue 
                uf.union(p, q)
            except ValueError:  
                print( "two integers between 0 and {}".format(n-1))
            
            print( "{} {}".format(p, q))
    
        print( "{} components".format(uf.count))


class Percolation(object):
  
    def __init__(self, n): # create n-by-n grid, with all sites blocked
  
        if (n <= 0): 
            raise ValueError("must be a number bigger than 0")
        
        self.site = [[0 for j in range(n+1)] for i in range(n+1)] # make an 2d array and populate in with '0', prog needs 1..n indexes
        self.n = n
        self.size = n * n + 2
        self.filled = self.size - 2
        self.toFill = self.size - 1
            
        self.id = WeightedQuickUnionUF(self.size)
        self.opened = 0
  
    def open(self, row, col):    # open site (row, col) if it is not open already
        self.validate(row, col)
        if not self.isOpen(row, col):
            p = self.trans(row, col)
  # System.out.println("opened id: " + p);
            if (row == self.n): 
                self.id.union(p, self.toFill) 

            if (row == 1): 
                self.id.union(self.filled, p)
            else:
                for neighbor in self.neighbors(row, col):
                    n = self.trans(neighbor)
                    if self.isFull(neighbor):
                        self.id.union(p, n)
                        break
                    
                    if self.isOpen(neighbor):
                        self.id.union(p, n) 
                      
            if self.isFull(row, col):
                for neighbor in self.neighbors(row, col):
                    n = self.trans(neighbor)
                    if self.isOpen(neighbor) and not self.isFull(neighbor):
                        self.id.union(n, p) 
                
            self.opened += 1
            self.site[row][col] = 1
        
    def isOpen(self, *args): # is site (row, col) open?
        if len(args) == 1:
            row, col = args[0]
        else:
            row, col = args
            
        self.validate(row, col)
        return self.site[row][col] == 1
   
    def isFull(self, *args):  # is site (row, col) full?
        if len(args) == 1:
            row, col = args[0]
        else:
            row, col = args
    
        self.validate(row, col)
        p = self.trans(row, col)
        return self.id.connected(p, self.filled)
  
    def numberOfOpenSites(self):       # number of open sites
        return self.opened
  
    def percolates(self):              # does the system percolate?
# //        for (int j = 1; j <= high; j++) {
# //            if (isFull(high, j)) return true; 
# //        }
        return self.id.connected(self.toFill, self.filled)
  
    @classmethod
    def test(cls,n):
        random.seed()
        perc = Percolation(n)
        key = "1"
        while not perc.percolates() and key != "":
            row = random.randint(1,n)
            col = random.randint(1,n)
            print( "row: {}, col: {}".format(row, col))
          
            try:
                perc.open(row, col)
            except ValueError:  
                print( "must be two integers between 1 and {}".format(n))
            print( "Opened sites:   {}".format(perc.numberOfOpenSites()))
            print( "Percolates:     {}".format(perc.percolates()))
          
            key = input() 
        
    
        print(perc.site)

    def validate(self, row, col):
        forRow = row <= 0 or row > self.n
        forCol = col <= 0 or col > self.n
        if forRow or forCol:
            raise ValueError 

    def trans(self, *args):
        if len(args) == 1:
            row, col = args[0]
        else:
            row, col = args
    
        return int(self.n * (row - 1) + (col - 1) % self.n)
  
    def transB(self, n):    
        i = n // self.n + 1
        j = n % self.n + 1
        return i,j

    def neighbors(self, row, col):
        neighbor = []
    
        for y in range(max(1, col - 1), min(col + 1, self.n)+1):
            # neighbor.push(trans(row, y)) if (y != col)
            if y != col:
                neighbor.append([row, y]) 
    
        for x in range(max(1, row-1),min(row+1, self.n)+1):
            # neighbor.push(trans(x, col))  if (x != row)
            if x != row:
                neighbor.append([x, col])  
    
        return neighbor

class PercolationStats(object):
  
    def __init__(self, n, numtrials):    # perform trials independent experiments on an n-by-n grid
    #Exception if either n <= 0 or trials <= 0
   
        self.trials = numtrials;
        if (n <= 0 or self.trials <= 0):
            raise ValueError("num and trials must be a number bigger than 0")
    
        self.experiments = [];
        random.seed()

        for i in range(self.trials):  
          perc = Percolation(n)
          while not perc.percolates():
            row = random.randint(1,n)
            col = random.randint(1,n)
           
            perc.open(row, col)
           
          self.experiments.append(perc.numberOfOpenSites()/(n*n))

    def mean(self):                          # sample mean of percolation threshold
        summ = sum(self.experiments)
        avg = summ / float(len(self.experiments))
        return avg
    def stddev(self):                       # sample standard deviation of percolation threshold
        m = self.mean()
        sqd = sum((x-m)**2 for x in self.experiments)
        stdev = (sqd / len(self.experiments) ) ** 0.5
        return stdev
    def confidenceLo(self):                 # low  endpoint of 95% confidence interval
        return self.mean() - (1.96*self.stddev())/(self.trials)**0.5
    
    def confidenceHi(self):                  # high endpoint of 95% confidence interval
        return self.mean() + (1.96*self.stddev())/(self.trials)**0.5

    @classmethod
    def test(cls, *args):      # test client (described below)
        n, trials = args
        #n = 20;
        #int trials = 100;
        start = cls(n, trials)
        
        print( "mean                    = {}".format(start.mean()))
        print( "stddev                  = {}".format(start.stddev()))
        print( "95% confidence interval = [{}, {}]".format(start.confidenceLo(), start.confidenceHi()))
