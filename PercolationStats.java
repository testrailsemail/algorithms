import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {
  private int n;
  private double[] experiments;
  private int trials;
  public PercolationStats(int num, int numtrials)    // perform trials independent experiments on an n-by-n grid
//       java.lang.IllegalArgumentException if either n <= 0 or trials <= 0
   {
    trials = numtrials;
    n = num;
    if (n <= 0 || trials <= 0) throw new java.lang.IllegalArgumentException("num and trials must be a number bigger than 0");

    experiments = new double[trials];
    
    for (int trial = 0; trial < trials; trial++) {
      Percolation perc = new Percolation(n);
      while (!perc.percolates()) {
        int row = StdRandom.uniform(1, n+1);
        int col = StdRandom.uniform(1, n+1);
        perc.open(row, col);
//        if ( perc.percolates() ) {
//        System.out.println("Open sites: " + 1.0*perc.numberOfOpenSites()/(n*n)); 
//      }
      } 
      experiments[trial] = (1.0*perc.numberOfOpenSites())/(n*n);
    }
   }
   public double mean()                          // sample mean of percolation threshold
   {
     return StdStats.mean(experiments);
   }
   public double stddev()                        // sample standard deviation of percolation threshold
   {
     return StdStats.stddev(experiments);
   }
   public double confidenceLo()                  // low  endpoint of 95% confidence interval
   {
     return mean() - (1.96*stddev())/Math.sqrt(1.0*trials);
   }
   public double confidenceHi()                  // high endpoint of 95% confidence interval
   {
     return mean() + (1.96*stddev())/Math.sqrt(1.0*trials);
   }

   public static void main(String[] args)        // test client (described below)
   {
     int n = Integer.parseInt(args[0]);
     int trials = Integer.parseInt(args[1]);
//    int n = 20;
//    int trials = 100;
     PercolationStats start = new PercolationStats(n, trials);
     System.out.println("mean                    = " + start.mean() + ";");
     System.out.println("stddev                  = " + start.stddev() + ";");
     System.out.println("95% confidence interval = " + "[" + start.confidenceLo() + ", " + start.confidenceHi()+ "]");

   }
}