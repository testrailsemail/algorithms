class Que
  class Node
    attr_accessor :next_item, :item
    def initialize(item = nil, next_item = nil)
      @item = item
    end
  end

  def initialize
    @first = nil
    @last  = nil
    @n = 0
  end

  def empty?
    @first == nil;
  end

  def size
    @n
  end

  def peek
    raise ArgumentError.new("Queue underflow") if empty?
    @first.item
  end

  def enqueue(item)
    oldlast = @last
    @last =  Node.new(item)
    if empty?
      @first = @last
    else
      oldlast.next_item = @last
    end
    @n += 1
    nil
  end

  def dequeue
    raise ArgumentError.new("Queue underflow") if empty?
    item = @first.item
    @first = @first.next_item
    @n -= 1
    @last = nil if empty?   # // to avoid loitering
    item
  end

  def each
    return enum_for(:each) unless block_given?
    iter = iterator

    while true
      begin
        vs = iter.next
      rescue StopIteration
        return $!.result
      end
      yield vs
    end
  end

  def to_s
    s = ""
    for item in self
      s << "#{item} "
    end
    s.chop
  end

  # /**
  #  * Returns an iterator that iterates over the items in this queue in FIFO order.
  #  *
  #  * @return an iterator that iterates over the items in this queue in FIFO order
  #  */
  def iterator
     ListIterator.new(@first)
  end

  # // an iterator, doesn't implement remove() since it's optional
  class ListIterator
    def initialize(first)
      @current = first
    end

    def hasNext?
      @current != nil
    end
    def remove
      raise "UnsupportedOperationException"
    end

    def next
      raise StopIteration.new if !hasNext?
      item = @current.item
      @current = @current.next_item
      item
    end
  end

  # /**
  #  * Unit tests the {@code Queue} data type.
  #  *
  #  * @param args the command-line arguments
  #  */
  def self.main(args)
    queue =  Queue.new
    while ((inp = gets.chomp) != "q")
      item = inp
      if item != "-"
        queue.enqueue(item)
      elsif !queue.empty?
        puts "#{queue.dequeue} "
      end
    end
    puts "(#{queue.size} left on queue"
  end
end
