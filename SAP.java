import java.util.HashMap;
import java.util.Map;
import edu.princeton.cs.algs4.BreadthFirstDirectedPaths;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class SAP {
  private final Digraph graph;
  private final Map<String, SAPProcessor> cache;

  // constructor takes a digraph (not necessarily a DAG)
  public SAP(Digraph g) {
    cache = new HashMap<>();
    graph = new Digraph(g);    
  }
  
  private boolean validIndex(Integer i) {
    if (i == null) return false;
    if (i < 0 || i >= graph.V())
      return false;
    return true;
  }

  private boolean validIndex(Iterable<Integer> vertices) {
    if (vertices == null) return false;
    for (Integer vertex : vertices)
      if (!validIndex(vertex))
        return false;
    return true;
  }

  // length of shortest ancestral path between v and w; -1 if no such path
  public int length(int v, int w) {
    if (!validIndex(v) || !validIndex(w))
      throw new java.lang.IllegalArgumentException();
    return cachedResult(v, w).distance;
  }
 

  // a common ancestor of v and w that participates in a shortest ancestral path; -1 if no such path
  public int ancestor(int v, int w) {
    if (!validIndex(v) || !validIndex(w))
      throw new java.lang.IllegalArgumentException();
    return cachedResult(v, w).ancestor;
  }

  // length of shortest ancestral path between any vertex in v and any vertex in w; -1 if no such path
  public int length(Iterable<Integer> v, Iterable<Integer> w) {
    if (!validIndex(v) || !validIndex(w))
      throw new java.lang.IllegalArgumentException();
    return cachedResult(v, w).distance;
  }

  // a common ancestor that participates in shortest ancestral path; -1 if no such path
  public int ancestor(Iterable<Integer> v, Iterable<Integer> w) {
    if (!validIndex(v) || !validIndex(w))
      throw new java.lang.IllegalArgumentException();
    return cachedResult(v, w).ancestor;
  }

  // do unit testing of this class
  public static void main(String[] args) {
    In in = new In(args[0]);

    Digraph G = new Digraph(in);
    SAP sap = new SAP(G);
    while (!StdIn.isEmpty()) {
      int v = StdIn.readInt();
      int w = StdIn.readInt();
//      int[] length   = sap.ETT();
//      int[] ancestor = sap.ETT();
      int length = sap.length(v, w);
      int ancestor = sap.ancestor(v, w);
      StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);

//      for (int n:G.reverse().adj(5)) StdOut.printf("length = %d", n);
//      System.out.println();
//      StdOut.printf("length = %d", G.E());
    }
  }
  
  private SAPProcessor cachedResult(int v, int w) {
    String key = v + "_" + w;
    if (cache.containsKey(key)) {
      SAPProcessor p = cache.get(key);
      // we need to cache only for 2 consecutive calls (distance and path), therefore we delete the result after the second call
      cache.remove(key);
      return p;
    }
    SAPProcessor p = new SAPProcessor(v, w);
    cache.put(key, p);
    return p;
  }

  private SAPProcessor cachedResult(Iterable<Integer> v, Iterable<Integer> w) {
    String key = v.toString() + "_" + w.toString();
    if (cache.containsKey(key)) {
      SAPProcessor p = cache.get(key);
      // we need to cache only for 2 consecutive calls (distance and path), therefore we delete the result after the second call
      cache.remove(key);
      return p;
    }

    SAPProcessor p = new SAPProcessor(v, w);
    cache.put(key, p);
    return p;
  }

  private class SAPProcessor {
    private int ancestor;
    private int distance;

    public SAPProcessor(int v, int w) {
      BreadthFirstDirectedPaths a = new BreadthFirstDirectedPaths(graph, v);
      BreadthFirstDirectedPaths b = new BreadthFirstDirectedPaths(graph, w);

      process(a, b);
    }

    public SAPProcessor(Iterable<Integer> v, Iterable<Integer> w) {
      BreadthFirstDirectedPaths a = new BreadthFirstDirectedPaths(graph, v);
      BreadthFirstDirectedPaths b = new BreadthFirstDirectedPaths(graph, w);

      process(a, b);
    }

    private void process(BreadthFirstDirectedPaths a, BreadthFirstDirectedPaths b) {
//      List<Integer> ancestors = new ArrayList<>();
      MinPQ<Integer> ancestors = new MinPQ<Integer>();
      Map<Integer, Integer> shortestAncestor = new HashMap<>();
      for (int i = 0; i < graph.V(); i++) {
        if (a.hasPathTo(i) && b.hasPathTo(i)) {
          int dist = a.distTo(i) + b.distTo(i);
          ancestors.insert(dist);
          shortestAncestor.put(dist, i);
        }
      }
      
      if (ancestors.isEmpty()) {
        distance = -1;
        ancestor = -1;
      }
      else {
        distance = ancestors.min();
        ancestor = shortestAncestor.get(distance);
      }
      
//      int shortestAncestor = -1;
//      int minDistance = Integer.MAX_VALUE;
//      for (int anc: ancestors) {
//        int dist = a.distTo(anc) + b.distTo(anc);
//        if (dist < minDistance) {
//          minDistance = dist;
//          shortestAncestor = anc;
//        }
//      }
//      if (Integer.MAX_VALUE == minDistance) {
//        distance = -1;
//      } else {
//        distance = minDistance;

//      }
//      ancestor = shortestAncestor;
    }
}
}