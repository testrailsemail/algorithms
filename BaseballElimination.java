import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import edu.princeton.cs.algs4.FlowEdge;
import edu.princeton.cs.algs4.FlowNetwork;
import edu.princeton.cs.algs4.FordFulkerson;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class BaseballElimination {
  private  Map<String, Integer> teams;
  private final int n;
  private final int[] wins;
  private final int[] loses;
  private final int[] remain;
  private final int[][] games;
  private int leaderWins;
  private String leader;
  
  public BaseballElimination(String filename) {
    // create a baseball division from given filename in format specified below
    In in = new In(filename);
//    while (!file.isEmpty()) {
    n = in.readInt();
    teams = new HashMap<>();
    wins = new int[n];
    loses = new int[n];
    remain = new int[n];
    games = new int[n][n];
    leaderWins = 0;
    for (int i = 0; i < n; i++) {
      String teamName = in.readString();
      teams.put(teamName, i);
      wins[i] = in.readInt();
      loses[i] = in.readInt();
      remain[i] = in.readInt();
      for (int j = 0; j < n; j++) 
        games[i][j] = in.readInt();
      if (wins[i] > leaderWins) {
        leaderWins = wins[i];
        leader = teamName;
      }
    }
  }
  
  private boolean triviallyEliminated(int id) {
    if (wins[id] + remain[id] < leaderWins)
      return true;
    return false;
  }
  
  public              int numberOfTeams() {
    // number of teams
    return n;
  }
  
  public Iterable<String> teams() {
    // all teams
    return teams.keySet();
  }
  
  public              int wins(String team) {
    // number of wins for given team
    if (team == null)
      throw new IllegalArgumentException("team could not be null");
    if (!teams.containsKey(team))
      throw new IllegalArgumentException("Please specify a valid team name");
    return wins[teams.get(team)];
  }
  
  public              int losses(String team) {
    // number of losses for given team
    if (team == null)
      throw new IllegalArgumentException("team could not be null");
    if (!teams.containsKey(team))
      throw new IllegalArgumentException("Please specify a valid team name");
    return loses[teams.get(team)];
  }
  
  public              int remaining(String team) {
    // number of remaining games for given team
    if (team == null)
      throw new IllegalArgumentException("team could not be null");
    if (!teams.containsKey(team))
      throw new IllegalArgumentException("Please specify a valid team name");
    return remain[teams.get(team)];
  }
  
  public              int against(String team1, String team2) {
    // number of remaining games between team1 and team2
    if (team1 == null || team2 == null)
      throw new IllegalArgumentException("team could not be null");
    if (!teams.containsKey(team1) || !teams.containsKey(team2))
      throw new IllegalArgumentException("Please specify a valid team name");
    return games[teams.get(team1)][teams.get(team2)];
  }
  
  public          boolean isEliminated(String team) {
    // is given team eliminated?
    if (team == null)
      throw new IllegalArgumentException("team could not be null");
    if (!teams.containsKey(team))
      throw new IllegalArgumentException("Please specify a valid team name");
    int id = teams.get(team);
    if (triviallyEliminated(id))
      return true;
    Graph graph = graphFor(id);
    for (FlowEdge edge: graph.network.adj(n))
      if (edge.capacity() - edge.flow() > 0)
        return true;
    return false;
    
  }
  
  public Iterable<String> certificateOfElimination(String team) {
    // subset R of teams that eliminates given team; null if not eliminated
    if (team == null)
      throw new IllegalArgumentException("team could not be null");
    if (!teams.containsKey(team))
      throw new IllegalArgumentException("Please specify a valid team name");
    int id = teams.get(team);
    
    Set<String> certificate = new HashSet<>();
    
    if (triviallyEliminated(id)) {
      certificate.add(leader);
      return certificate;
    }
        
    Graph graph = graphFor(id);
    for (FlowEdge edge: graph.network.adj(graph.source))
      if (edge.capacity() - edge.flow() > 0) {
        for (String teamName: teams())
          if (graph.ff.inCut(teams.get(teamName)))
            certificate.add(teamName);
        return certificate;
      }
    
    return null;
  }
  
  private Graph graphFor(int id) {
    int start = n;
    int sink = n + 1;
    int gameNode = n + 2;
    Set<FlowEdge> edgeSet = new HashSet<>();
    for (int i = 0; i < n - 1; i++) {
      if (i != id && !triviallyEliminated(i)) {
        for (int j = i + 1; j < n; j++) {
          if (j != id && !triviallyEliminated(j) && games[i][j] != 0) {
            edgeSet.add(new FlowEdge(start, gameNode, games[i][j]));
            edgeSet.add(new FlowEdge(gameNode, i, Double.POSITIVE_INFINITY));
            edgeSet.add(new FlowEdge(gameNode, j, Double.POSITIVE_INFINITY));
            gameNode++;
          }
        }
        edgeSet.add(new FlowEdge(i, sink, wins[id]+ remain[id] - wins[i]));
      }
    }
    if (!triviallyEliminated(n-1) && n-1 != id)
      edgeSet.add(new FlowEdge(n-1, sink, wins[id]+ remain[id] - wins[n-1]));
    FlowNetwork network = new FlowNetwork(gameNode);
    for (FlowEdge edge : edgeSet)
      network.addEdge(edge);
    FordFulkerson ff = new FordFulkerson(network, n, n+1);
    Graph graphOf = new Graph(ff, network, start, sink);
    return graphOf;
  }
  
  public static void main(String[] args) {
    BaseballElimination division = new BaseballElimination(args[0]);
//    StdOut.println(division.graph(1).toString());
    for (String team : division.teams()) {
      if (division.isEliminated(team)) {
        StdOut.print(team + " is eliminated by the subset R = { ");
        for (String t : division.certificateOfElimination(team)) {
          StdOut.print(t + " ");
        }
        StdOut.println("}");
      }
      else {
        StdOut.println(team + " is not eliminated");
      }
    }
  }
  
  private class Graph {
    private final FordFulkerson ff;
    private final FlowNetwork network;
    private final int source;
    private final int sink;

    public Graph(FordFulkerson ff, FlowNetwork network, int source, int sink) {
        super();
        this.ff = ff;
        this.network = network;
        this.source = source;
        this.sink = sink;
    }
  }
}