import java.util.Arrays;
import java.util.HashMap;
// import java.util.HashSet;
import java.util.Map;
// import java.util.Map.Entry;

// import edu.princeton.cs.algs4.StdOut;

public class BruteCollinearPoints {
//  private int segments;
  private LineSegment[] segmentsList;
   public BruteCollinearPoints(Point[] points) {    // finds all line segments containing 4 points
     if (points == null)
       throw new NullPointerException("one of the points is null");
//     HashSet<String> set = new HashSet<String>();
//     HashSet<String> set1 = new HashSet<String>();

//   for (Point point: points) {    
     for (int j = 0; j < points.length; j++) {
       if (points[j] == null)
       throw new NullPointerException("one of the points is null");
     
       for (int k = j+1; k < points.length; k++)
         if (k != j && points[k].compareTo(points[j]) == 0)
           throw new IllegalArgumentException("one of the points is repeated");
//       boolean p = set.add(point.toString());
//       if (!p)
//         throw new IllegalArgumentException("one of the points is repeated");
     }
//     set = new HashSet<Point>();
     
//     segments = 0;
     int n = points.length;
     Map<String, LineSegment> map = new HashMap<String, LineSegment>();
//     HashSet<LineSegment> setSeg = new HashSet<LineSegment>();
     
     Point[] pointsC = new Point[n];
     System.arraycopy(points, 0, pointsC, 0, n);
     
     for (Point i : points) {
       Arrays.sort(pointsC, i.slopeOrder());
       for (int j = 1; j <= n-3; j++) {
         double slope1 = pointsC[0].slopeTo(pointsC[j]);
         double slope2 = pointsC[0].slopeTo(pointsC[j+1]);
         double slope3 = pointsC[0].slopeTo(pointsC[j+2]);
         
         if (slope1 == slope2 && slope1 == slope3 && slope2 == slope3) {
           Point[] arr = {pointsC[0], pointsC[j], pointsC[j+1], pointsC[j+2]};
           Arrays.sort(arr);
//           String arry = "";
           StringBuilder arrys = new StringBuilder();
           for (Point point : arr) arrys.append(point.toString());
           String arry = arrys.toString();
//           System.out.println("SLOPES for " + i+ " : "+ arr[0] + " " +arr[1]+ " " +arr[2]+ " " +arr[3]);
           LineSegment line = new LineSegment(arr[0], arr[3]);
//           boolean p = set1.add(arry);
           
//           if (p) {
//             setSeg.add(line);
             map.put(arry, line);
//             segments++;
//           }
           
         }
         
       }
       
     }
//     System.out.println(set1);
//     set1 = new HashSet<String>();
//     segmentsList = setSeg.toArray(new LineSegment[setSeg.size()]);
     segmentsList = map.values().toArray(new LineSegment[0]);
//     System.out.println(setSeg.size() + " SIZE ");
//     System.out.println(numberOfSegments());
//     setSeg = new HashSet<LineSegment>();
//     for (Entry<String, Point[]> entry: map.entrySet()) {
//       System.out.println(entry.getKey());
//       for (Point point : entry.getValue())
//         System.out.print(point + " ");
//       StdOut.println();
//     }
//     map = new HashMap<String, LineSegment>();
   }
   public int numberOfSegments() {       // the number of line segments
     return segmentsList.length;
   }
   public LineSegment[] segments() {               // the line segments
    return segmentsList.clone();
   }
}