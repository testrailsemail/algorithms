import java.awt.Color;

import edu.princeton.cs.algs4.Picture;
import edu.princeton.cs.algs4.StdOut;

public class SeamCarver {
  private static final int MAX_ENERGY = 1000;
  private Picture pic;
  private int[][] picArray;
  private int[] seams; 
//  private double[][] energyPath;
//  private double[][] energy;
  private boolean transposed;
  private boolean transformed;
  private boolean updated;
  private int preTransposedHeight, preTransposedWidth;
  
  
  public SeamCarver(Picture picture) {               // create a seam carver object based on the given picture
    if (picture == null)
      throw new IllegalArgumentException("Pic could not be null");
    int h = picture.height();
    int w = picture.width();
    preTransposedHeight = h;
    preTransposedWidth = w;
    picArray = new int[h][w];
//    energy = new double[h][w];
    transposed = false;
    transformed = false;
    updated = true;
    for (int row = 0; row < h; row++)
      for (int col = 0; col < w; col++)
        picArray[row][col] = picture.getRGB(col, row);
    findSeam();
  }
  
  private int picHeight() {
    return picArray.length;
  }
  
  private int picWidth() {
    return picArray[0].length;
  }
  
  private Picture createPicture() {
    Picture picture = new Picture(picWidth(), picHeight());
    for (int col = 0; col < picWidth(); col++)
      for (int row = 0; row < picHeight(); row++)
        picture.setRGB(col, row, picArray[row][col]);
    return picture;
  }
  
  public Picture picture() {                         // current picture
    checkTransposed();
    checkUpdated();
    return new Picture(pic);
  }
  
  public     int width() {                           // width of current picture
    return preTransposedWidth;
  }
  
  public     int height() {                          // height of current picture
    return preTransposedHeight;
  }
  
  public  double energy(int x, int y) {              // energy of pixel at column x and row y
    checkTransposed();
    return getEnergy(x, y, picArray);
  }
  
  private  double getEnergy(int x, int y, int[][] p) {
    int h = p.length;
    int w = p[0].length;
    validateIndex(x, w);
    validateIndex(y, h);
    if (x == 0 || x == w - 1 || y == 0 || y == h - 1)
      return MAX_ENERGY;
    double xEnergy, yEnergy;
    xEnergy = pixelEnergy(p[y][x-1], p[y][x+1]);
    yEnergy = pixelEnergy(p[y-1][x], p[y+1][x]);
//    xEnergy = pixelEnergy(pic.get(x-1, y), pic.get(x+1, y));
//    yEnergy = pixelEnergy(pic.get(x, y-1), pic.get(x, y+1));
    return Math.sqrt(xEnergy + yEnergy);
  }
  
  private  double getEnergy(int col, int row) {
    return getEnergy(col, row, picArray);
  }
  
  private double pixelEnergy(int m, int n) {
    int red, green, blue;
    Color a = new Color(m);
    Color b = new Color(n);
    red = a.getRed() - b.getRed();
    green = a.getGreen() - b.getGreen();
    blue = a.getBlue() - b.getBlue();
    return red*red + blue*blue + green*green;
  }
  
// throw an IllegalArgumentException unless {@code 0 <= x <= W-1}
  private void validateIndex(int x, int max) {
    if (x < 0 || x >= max)
      throw new IllegalArgumentException("index " + x + " is not between 0 and " + (max-1));
  }
  
  private void transpose() {
    int[][] transposedPicture = new int[picWidth()][picHeight()];
    
//    double[][] newEnergyArray = new double[picWidth()][picHeight()];
//    int[][] newParentOf = new int[picWidth()][picHeight()];
//    double[][] newEnergy = new double[picWidth()][picHeight()];
    for (int w = 0; w < picWidth(); w++)
      for (int h = 0; h < picHeight(); h++) {
        transposedPicture[w][h] = picArray[h][w];
//        newEnergy[w][h] = energy[h][w];
      }
//    parentOf = newEnergyArray;
//    energy = newEnergy;
    picArray = transposedPicture;
    transformed = true;
//    StdOut.printf("ha: %d, w: %d; ", energyPath.length, energyPath[0].length);
//    parent = new int[picture.width()][picture.height()];
  }
  
  private void checkTransposed() {
    if (transposed) {
      transpose();
      transposed = false;
    }
  }
  
  private void checkUpdated() {
    if (updated) {
      pic = createPicture();
      updated = false;
    }
  }
  
  public   int[] findHorizontalSeam() {              // sequence of indices for horizontal seam
    if (!transposed)
      transpose();
//    int[] seam = findSeam(s, e);
    if (transformed) { 
      findSeam();
      transposed = true;
      transformed = false;
    }
    return seams.clone();
  }
  
  public   int[] findVerticalSeam() {
//    checkTransposed();
    if (transposed) 
      transpose();
    if (transformed) {
      findSeam();
      transposed = false;
      transformed = false;
    }
    return seams.clone();
  }
  
  private   void findSeam() {                // sequence of indices for vertical seam
    int height = picHeight(); // height();
    int width = picWidth(); // width();
    int[][] parentOf = new int[height][width];
    double[] distTo = new double[width];
    double[] prevDistTo = new double[width];

    seams = new int[height];

    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++)
        relaxVertically(x, y, parentOf, distTo, prevDistTo);
      System.arraycopy(distTo, 0, prevDistTo, 0, width);
    }
    double min = prevDistTo[0]; // energyPath[height - 1][0];
    for (int index = 0; index < prevDistTo.length; index++)
      if (prevDistTo[index] <= min) {
        min = prevDistTo[index]; // energyPath[height - 1][index];
        seams[height - 1] = index;
      }
    
    for (int y = height - 2; y >= 0; y--) {
      seams[y] = parentOf[y+1][seams[y+1]];
    }
  }
  
  private void relaxVertically(int col, int row, int[][] parentOf, double[] distTo, double[] prevDistTo) {
//    if (transposed || energy[picHeight()-1][picWidth()-1] == 0)
//      energy[row][col] = getEnergy(col, row);
    
    if (row == 0) {
      distTo[col] = getEnergy(col, row); // energy[row][col];
      parentOf[row][col] = -1;
      return;
    }

    if (col == picWidth() - 1) {
      // 2 edges
      double a = prevDistTo[col];
      double b = prevDistTo[Math.max(col-1, 0)];
      double min;
      if (a < b) {
        parentOf[row][col] = col;
        min = a;
      } else {
        parentOf[row][col] = Math.max(col-1, 0);
        min = b;
      }
      distTo[col] = min + getEnergy(col, row);
      return;
    }
    
    if (col == 0) {
      // 2 edges
      double a = prevDistTo[col];
      double b = prevDistTo[col+1];
      double min;
      if (a < b) {
        parentOf[row][col] = col;
        min = a;
      } else {
        parentOf[row][col] = col + 1;
        min = b;
      }
      distTo[col] = min + getEnergy(col, row);
      return;
    }
    
    // for 3 edges
    double left = prevDistTo[col - 1];
    double mid = prevDistTo[col];
    double right = prevDistTo[col + 1];
    double min = Math.min(Math.min(left, mid), right);
    if (min == mid) {
      parentOf[row][col] = col;
    } else if (min == right) {
      parentOf[row][col] = col + 1;
    } else {
      parentOf[row][col] = col - 1;
    }
    distTo[col] = min + getEnergy(col, row);
//    if (row == picHeight()-1 && col == 300) {
//      StdOut.printf("min:::%7.2f row: %d-> col: %d; ", min, row, col);
//      StdOut.printf("a:::%7.2f b: %7.2f; c: %7.2f; ", left, mid, right);
//      StdOut.println();
//    }
  }
  
//  private void validateSeam(int[] seam) {
////    StdOut.printf("S-len: %d; ", seam.length);
////  StdOut.println();
//    if (seam.length != picHeight())
//      throw new IllegalArgumentException("seam is of the wrong size ");
//    for (int i = 0; i < seam.length - 1; i++) {
//      if (Math.abs(seam[i] - seam[i + 1]) > 1)
//        throw new IllegalArgumentException();
//      validateIndex(seam[i], picWidth());
//    }
//    validateIndex(seam[seam.length - 1], picWidth());
//  }
  
  public    void removeHorizontalSeam(int[] seam) {  // remove horizontal seam from current picture
    if (seam == null)
      throw new IllegalArgumentException("seam could not be null");
//    StdOut.printf("A-h: %d, A-w: %d; E-h: %d, E-w: %d;", 
//    picArray.length, picArray[0].length, energyPath.length, energyPath[0].length);
//    StdOut.println();
//    StdOut.printf("S-l: %d;", seam.length);
//    StdOut.println();
    if (!transposed) {
      transpose();
      transposed = true;
    }
//    if (picHeight() <= 1)
//      throw new IllegalArgumentException("The picture is too small");
//    transposed = false;
    removeSeam(seam);
//    removeVerticalSeam(seam);
    
    preTransposedWidth = picHeight();
    preTransposedHeight = picWidth();
  }
  
  public    void removeVerticalSeam(int[] seam) {    // remove vertical seam from current picture
    if (seam == null)
      throw new IllegalArgumentException("seam could not be null");
//    checkTransposed();
    if (transposed) {
      transpose();
      transposed = false;
    }
//    StdOut.printf("S-len: %d; p-H: %d; ", seam.length, picHeight());
//  StdOut.println();
    
//    validateSeam(seam);
    
    removeSeam(seam);
    
    preTransposedWidth = picWidth();
    preTransposedHeight = picHeight();
  }
  
  private void removeSeam(int[] seam) {
    if (picWidth() <= 1)
      throw new IllegalArgumentException("The picture is too small");
    if (seam.length != picHeight())
      throw new IllegalArgumentException("seam is of the wrong size ");
    for (int i = 0; i < seam.length - 1; i++) {
      if (Math.abs(seam[i] - seam[i + 1]) > 1)
        throw new IllegalArgumentException();
      validateIndex(seam[i], picWidth());
    }
    validateIndex(seam[seam.length - 1], picWidth());
    
    int[][] p = new int[picHeight()][picWidth() - 1];
//    double[][] e = new double[picHeight()][picWidth() - 1];
//    double[][] ep = new double[picHeight()][picWidth() - 1];
    for (int row = 0; row < picHeight(); row++) {
      int k = seam[row];
      System.arraycopy(picArray[row], 0,    p[row], 0, k);
      System.arraycopy(picArray[row], k+1,  p[row], k, picArray[row].length - k - 1);
//      System.arraycopy(energy[row],   0,    e[row], 0, k);
//      System.arraycopy(energy[row],   k+1,  e[row], k, energy[row].length - k - 1);
    }
    
    picArray = p;
//    for (int row = 0; row < picHeight(); row++) { 
//      int first =   Math.min(seam[row], picWidth()-1);
//      int second =  Math.max(seam[row]-1, 0);
//      e[row][first] = getEnergy(first, row, p);
//      e[row][second] = getEnergy(second, row, p);
//    }
//    energy = e;
    transformed = true;
    updated = true;
//    energyPath = ep;
//    StdOut.printf("A-h: %d, A-w: %d; p-h: %d, p-w: %d;", picArray.length, picArray[0].length, p.length, p[0].length);
//    StdOut.println();
    
  }
  
  public static void main(String[] args) {
    Picture picture = new Picture(args[0]);
    StdOut.printf("image is %d pixels wide by %d pixels high.\n", picture.width(), picture.height());
    
    SeamCarver sc = new SeamCarver(picture);
    
    StdOut.printf("Printing energy calculated for each pixel.\n");        

    for (int row = 0; row < sc.height(); row++) {
        for (int col = 0; col < sc.width(); col++)
            StdOut.printf("%9.0f ", sc.energy(col, row));
        StdOut.println();
    }
//    Picture picture = new Picture(args[0]);
//    StdOut.printf("image is %d columns by %d rows\n", picture.width(), picture.height());
//    picture.show();        
//    SeamCarver sc = new SeamCarver(picture);
    
//    StdOut.printf("Displaying energy calculated for each pixel.\n");
//    SCUtility.showEnergy(sc);
  }
}
