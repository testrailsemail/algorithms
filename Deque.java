import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.princeton.cs.algs4.StdOut;

public class Deque<Item> implements Iterable<Item> {
  private Node<Item> first;     // top of queue
  private Node<Item> last;      // top of queue
  private int n;                // size of the queue

  // helper linked list class
  private static class Node<Item> {
      private Item item;
      private Node<Item> next;
      private Node<Item> previous;
  }
  
  public Deque() {                           // construct an empty deque
    first = null;
    last = null;
    n = 0;
  }
  
  public boolean isEmpty() {                 // is the deque empty?
    return n == 0;
  }
  
  public int size() {                        // return the number of items on the deque
    return n;
  }
  
  public void addFirst(Item item) {          // add the item to the front
    validate(item);
    Node<Item> temp = first;
    first = new Node<Item>();
    first.item = item;
    first.next = temp;
    first.previous = null;
    if (temp != null) temp.previous = first;
    n++;
    if (size() == 1) last = first;
  }
  
  public void addLast(Item item) {           // add the item to the end
    validate(item);
    Node<Item> temp = last;
    last = new Node<Item>();
    last.item = item;
    last.next = null;
    last.previous = temp;
    if (temp != null) temp.next = last;
    n++;
    if (size() == 1) first = last;
  }
  
  private void validate(Item item) {
    if (item == null) throw new NullPointerException();
  }
  
  private void cantRemove() {
    if (isEmpty()) throw new NoSuchElementException();
  }
  
  private Item removeAll(Node<Item> item) {
    first = null;
    last = null;
    n = 0;
    return item.item;
  }
  
  public Item removeFirst() {               // remove and return the item from the front
    cantRemove();
    Node<Item> temp = first;
    if (size() == 1) return removeAll(first);
    first = first.next;
    first.previous = null;
    n--;
    return temp.item;
  }
  
  public Item removeLast() {                // remove and return the item from the end
    cantRemove();
    if (size() == 1) return removeAll(last);
    Node<Item> temp = last;
    last = last.previous;
    last.next = null;
    n--;
    return temp.item;
  }
  
//  public String toString() {
//    StringBuilder s = new StringBuilder();
//    for (Item item : this) {
//        s.append(item);
//        s.append(' ');
//    }
//    return s.toString();
//  }
  
  public Iterator<Item> iterator() {        // return an iterator over items in order from front to end
    return new ListIterator<Item>(first);
  }
  
  private class ListIterator<Item> implements Iterator<Item> {
    private Node<Item> current;

    public ListIterator(Node<Item> first) {
        current = first;
    }

    public boolean hasNext() {
        return current != null;
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }

    public Item next() {
        if (!hasNext()) throw new NoSuchElementException();
        Item item = current.item;
        current = current.next; 
        return item;
    }
  }
  
  public static void main(String[] args) {  // unit testing (optional)
    Deque<String> stack = new Deque<String>();
    
    stack.addFirst("a");
    stack.addLast("z");
    stack.removeFirst();
    stack.addFirst("b");
//    StdOut.println("(" + stack + ")");
        
    stack.removeLast();
       
    stack.addLast("y");
//    StdOut.println("(" + stack + ")");
    stack.removeLast();
    
    stack.removeLast();
    StdOut.println("(" + stack.size() + " left on stack)");
  }
}