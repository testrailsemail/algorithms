# -*- coding: utf-8 -*-
"""
Created on Thu Apr  6 11:28:53 2017

@author: admin
"""
import random
import sys

class Node(object):
    def __init__(self):
        self.item = None
        self.next = None
        self.previous = None

class Deque(object):

  def __init__(self):
    self.first = None
    self.last = None
    self.n = 0
      
  def isEmpty(self):
    return self.n == 0
  
  def size(self):                       # return the number of items on the deque
    return self.n
  
  def addFirst(self, item):              # add the item to the front
    self.validate(item)
    temp = self.first
    self.first = Node()
    self.first.item = item
    self.first.next = temp
    self.first.previous = None
    if temp != None:
        temp.previous = self.first 
    self.n += 1
    if self.size() == 1:
        self.last = self.first 
  
  
  def addLast(self, item):           # add the item to the end
    self.validate(item)
    temp = self.last
    self.last = Node()
    self.last.item = item
    self.last.next = None
    self.last.previous = temp
    if temp != None:
        temp.next = self.last 
    self.n += 1
    if self.size() == 1:
        self.first = self.last 
  
  def removeFirst(self):               # remove and return the item from the front
    self.canRemove()
    temp = self.first
    if self.size() == 1:
        return self.removeAll(self.first) 
    self.first = self.first.next
    self.first.previous = None
    self.n -= 1
    return temp.item
  
  def removeLast(self):               # remove and return the item from the end
    self.canRemove()
    if self.size() == 1:
        return self.removeAll(self.last) 
    temp = self.last
    self.last = self.last.previous
    self.last.next = None
    self.n -= 1
    return temp.item

  def __iter__(self):
      self.current = self.first
      return self
        
  def __next__(self):
    
    if self.current == None:
        raise StopIteration
    else:
        temp = self.current
        self.current = self.current.next
        return temp 
        
  def __str__(self):
    s = []
    for node in self:
        s.append(node.item)
    return str(s)

  @classmethod
  def test (cls, *args):   # unit testing (optional)
    stack = cls()
    
    stack.addFirst("a")
    stack.addLast("z")
    stack.removeFirst()
    stack.addFirst("b")
# //    StdOut.println("(" + stack + ")");
    stack.removeLast()
    stack.addLast("y")
# //    StdOut.println("(" + stack + ")");
    stack.removeLast()
    stack.removeLast()
    print( "(#{} left on stack)".format(stack.size()))
  
  def validate(self, item):
    if item == None:
        raise ValueError("You gave me nothing!")
  
  def canRemove(self):
    if self.isEmpty():
        raise ValueError("Deque is empty!") 
  
  def removeAll(self, item):
    self.first = None
    self.last = None
    self.n = 0
    return item.item
  
class RandomizedQueue(object):
  
  def __init__(self):                 # construct an empty randomized queue
    self.q =  [] # Array.new(2)
    random.seed()
    
  def isEmpty(self):                 # is the queue empty?
    return not self.q
    
  def size(self):                   # return the number of items on the queue
    return len(self.q)
      
  def enqueue(self, item):           # add the item
    if item == None:
        raise ValueError("You gave me nothing!")
    self.q.append(item)
    
  def dequeue(self):                    # remove and return a random item
    if self.isEmpty():
        raise ValueError("Queue is empty!") 
    i = random.randrange(self.size())
    return self.q.pop(i)
  
  def sample(self):                    # return (but do not remove) a random item
    if self.isEmpty():
        raise ValueError("Queue is empty!")
    return random.choice(self.q)
  
  def len(self):
    return len(self.q)
    
  def __str__(self):
    return str(self.q)
  
  def __iter__(self):
    t = self.q[:]
    random.shuffle(t)
    return iter(t)

  @classmethod
  def test(cls,*args):         # unit testing (optional)
    queue = cls()
    queue.enqueue("a")
    print( "(#{} is in queue)".format(queue.sample()))
    print( "removed:        #{}".format(queue.dequeue()))
    queue.enqueue("a")
    queue.enqueue("b")
    queue.enqueue("c")
    queue.enqueue("d")
    for i in queue: print( "element:        #{}".format(i))
    print( "num elements:   #{}".format(queue.len()))
    print( "elements:       #{}".format(queue))
    print( "elements:       ")
    for i in queue: print( "#{} ".format(i),)
    print()
    print( "array size:     #{}".format(queue.len()))


class Permutation(object):

  @classmethod
  def test (cls, *args):
#    print(args[0])    
    k = int(args[0])#.to_i
    
    n = 1
    a = [None] * k
    random.seed()
    item = 1    
    if k != 0:
      qu = RandomizedQueue()
      print( "anything or Enter key to abort")
      while (item):
        # input = gets  
        # item = input.chomp
        try: item = raw_input().rstrip()
        except EOFError: 
            break
        if not item: break 
        if (n <= k): 
          a[n-1] = item
          n +=1
        else: 
          j = random.randrange(n)
          if (j < k): a[j] = item 
          n += 1
        
      for i in a: qu.enqueue(i) 
      for i in qu: print( i)
      print(qu)
    

print(sys.argv)
Permutation.test(sys.argv.pop(1)) # 