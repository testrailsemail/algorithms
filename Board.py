import random
import heapq
import sys

class Board(object):
  
  def __init__(self, blocks):     # construct a board from an n-by-n array of blocks
    if blocks is None:
        raise ValueError( "You gave me a nil!") 

    self.n = len(blocks)
    self.tiles = []
#    if len(blocks) != len(set(blocks)):  
#        raise ValueError("one of the blocks is repeated")
    for arr in blocks:
      self.tiles.append(arr[:])
      if len(arr) != self.n:
        raise ValueError("wrong size") 
    
  
                                         
  def dimension(self):                 # board dimension n
    return self.n
  
  
  def hamming(self):                    # number of blocks out of place
    count = 0
    for i, val in enumerate(self.tiles): 
      for j, e in enumerate(val):
        if (e != 0 and e != 1 + self.n*i+j):
          count += 1
    return count  
  
  
  def manhattan(self):                # sum of Manhattan distances between blocks and goal
    count = 0
    for i, val in enumerate(self.tiles): 
      for j, e in enumerate(val):
        if (e != 0):
            count += abs((e - 1)//self.n - i) + abs((e - 1) % self.n - j)
    
    return count      
  
  
  def isGoal(self):               # is this board the goal board?
    goal = []
    for i in range(self.n):
      goal.append([])  
      for j in range(self.n):
        goal[i].append(1 + self.n*i+j)
    goal[self.n-1][self.n-1] = 0
    return self.tiles == goal
  
  
  def twin(self):                    # a board that is obtained by exchanging any pair of blocks
    arr = []
    pointers = []
    flag = 0
    for ar in self.tiles:
      arr.append(ar[:])
    random.seed()
#    row = random.randrange(self.n)
    while (flag != 1):
      pointers = [random.randrange(self.n) for i in range(4)]  
      p = (pointers[0] == pointers[2]) and (pointers[1] == pointers[3])
      if (not p and arr[pointers[0]][pointers[1]] != 0 and arr[pointers[2]][pointers[3]] != 0):
        arr[pointers[0]][pointers[1]], arr[pointers[2]][pointers[3]] = arr[pointers[2]][pointers[3]], arr[pointers[0]][pointers[1]]
        break
      
    twin1 = Board(arr)
    return twin1 
  
  def exch(self, a, i, j, k, l):
    b = []
    for ar in a:
      b.append(ar[:])
    
    b[i][j], b[k][l] = b[k][l], b[i][j]
    return b
  
  
  def equals(self, other):      # does this board equal y?
    if (other is self):
        return True  
    if (other is None):
        return False  
    if type(other) != type(self):
        return False  
    return self.tiles == other.tiles
  
  
  def neighbors(self):    # all neighboring boards
    neighbors = []
#    row = self.tiles.detect{|aa| aa.include?(0)}
#    col = row.index(0)
#    row = self.tiles.index(row)
    c = [(i, x.index(0)) for i, x in enumerate(self.tiles) if 0 in x]
    row, col = c[0]
    
    for y in range(max(0, col - 1), 1 + min(col + 1, self.n-1)):
      if (y != col):
          arr = self.exch(self.tiles, row, col, row, y) 
          neighbors.append(Board(arr))
    for x in range(max(0, row-1), 1 + min(row+1, self.n-1)):
      if (x != row):
          arr = self.exch(self.tiles, row, col, x, col) 
          neighbors.append(Board(arr))
    
    return neighbors
  
  
  def __str__(self):              # string representation of this board (in the output format specified below)
    st = str(self.n) + "\n"
    for row in self.tiles:
      for e in row:
          st = st + " " + str(e)
      st = st + "\n"
      
#    self.tiles.each { |row| row.each { |e| str << ("%2s " % e) }; str << "\n" }# row.join(" "); str << "\n"}
    return st
  
  @classmethod
  def test(cls, *args):  # unit tests (not graded)
    arr = []
    n = 3
    for i in range(n):
      arr.append([])  
      for j in range(n):
        arr[i].append(1 + n*i+j)
    
    arr[n-1][n-1] = 0
    bo = cls(arr)
    print("equality check: {}".format(bo.isGoal()))
    print("initial board:  {}".format(bo))
    for neighbor in bo.neighbors():
      print("neighbor board:  {}".format(neighbor))
    
    print("twin board:  {}".format(bo.twin()))
    print("initial board:  {}".format(bo))
    bo = bo.twin()
    print("twin board:  {}".format(bo))
    print("boardtwin manh #{}".format(bo.manhattan()))
    print("boardtwin ham #{}".format(bo.hamming()))
    bo = bo.twin()
    print("twintwintwin board:  #{}".format(bo))
    print("boardtwintwin manh #{}".format(bo.manhattan()))
    print("boardtwintwin ham #{}".format(bo.hamming()))



class Element(object):
  def __init__(self, name, priority):
    self.name, self.priority = name, priority
  
  def __lt__(self, other):
    return self.priority < other.priority
  
  def __eq__(self, other):
    return self.priority == other.priority


class MinPQ(object):
  
  def __init__(self):
    self.elements = [None]
  

  def append(self, element):
    self.elements.append(element)
    self.bubble_up(len(self.elements) - 1)
  

  def exchange(self, source, target):
    self.elements[source], self.elements[target] = self.elements[target], self.elements[source]
  

  def bubble_up(self, indeks):
    parent_index = indeks//2

    # return if we reach the root element
    if indeks <= 1: return 

    # or if the parent is already greater than the child
    if self.elements[parent_index] <= self.elements[indeks]: 
        return 

    # otherwise we exchange the child with the parent
    self.exchange(indeks, parent_index)

    # and keep bubbling up
    self.bubble_up(parent_index)
  

  def pop(self):
    # exchange the root with the last element
    self.exchange(1, len(self.elements) - 1)

    # remove the last element of the list
    max = self.elements.pop()

    # and make sure the tree is ordered again
    self.bubble_down(1)
    return max
  

  def bubble_down(self, index):
    child_index = (index * 2)
    # stop if we reach the bottom of the tree
    if child_index > len(self.elements) - 1:
      return 

    # make sure we get the largest child
    not_the_last_element = child_index < len(self.elements) - 1
    left_element = self.elements[child_index]
    if not_the_last_element:
        right_element = self.elements[child_index + 1]
        if right_element < left_element:
            child_index += 1 

    # there is no need to continue if the parent element is already bigger
    # then its children
    if self.elements[index] <= self.elements[child_index]:
        return 

    self.exchange(index, child_index)

    # repeat the process until we reach a point where the parent
    # is larger than its children
    self.bubble_down(child_index)

class Node(object):
    
    def __init__(self, item, moves, previousNode = None, priority = "manhattan"):
      self.board = item
      self.moves = moves
      self.previous = previousNode
      self.priority = self.priorityM() if priority == "manhattan" else self.priorityH()
    
    def priorityM(self):
      pri = self.moves + self.board.manhattan()
      return pri
     
    def priorityH(self):
      pri = self.moves + self.board.hamming()
      return pri

    def __lt__(self, other):
      return self.priority < other.priority
    
    def __eq__(self, other):
      return self.priority == other.priority
      
    def __le__(self, other):
      return self.priority <= other.priority


class Solver(object):
  def __init__(self, initial):           # find a solution to the initial board (using the A* algorithm)
    if (initial is None):
        raise ValueError( "null initial puzzle board")  
    
    self.moves = 0
    self.goalNode = None
    initialNode =  Node(initial, self.moves)
    initialTwin =  Node(initial.twin(), self.moves)

    pq =  MinPQ()
    pqTwin =  MinPQ()
      
    pq.append(initialNode)
    pqTwin.append(initialTwin)
      
    current = initialNode
    currentTwin = initialTwin
      
    initialSolved = False
    twinSolved = False
    
    movesTwin = 0

    while (not initialSolved and not twinSolved):
      current = pq.pop()
      if current is None:
        self.isSolvable = False
        self.moves = -1
        break
      
      self.moves = current.moves
      currentTwin = pqTwin.pop()
      movesTwin = currentTwin.moves
      if current.board.isGoal(): 
        self.isSolvable = True
        initialSolved = True
        self.goalNode = current
        break
      
      if currentTwin.board.isGoal() or twinSolved:
        twinSolved = True
        self.isSolvable = False
        self.moves = -1
        break
      

      self.moves += 1
      movesTwin += 1

      for n in current.board.neighbors():
        temp = current
        flag = True
        while  temp.previous is not None:
          if n.equals(temp.previous.board):
            flag = False;
            break
          
          temp = temp.previous
        
        if flag:
            pq.append(Node(n, self.moves, current)) 
      
      if not twinSolved: 
        for n in currentTwin.board.neighbors():
          flag = True
          temp = currentTwin
          while  temp.previous is not None:
            if n.equals(temp.previous.board): 
              flag = False
              break
            
            temp = temp.previous
          if flag:
              pqTwin.append(Node(n, movesTwin, currentTwin)) 

    # private Comparator<Node> manhattan() {
    #   return new Comparator<Node>() {
    #     self.Override
    #     public int compare(Node o1, Node o2) {
    #         return Integer.compare(o1.priorityM(), o2.priorityM());
    #     }
    #   };
    # }
    
    # private Comparator<Node> hamming() {
    #   return new Comparator<Node>() {
    #     self.Override
    #     public int compare(Node o1, Node o2) {
    #         return Integer.compare(o1.priorityH(), o2.priorityH());
    #     }
    #   };
    # }
    
  def isSolvable(self):            # is the initial board solvable?
    return self.isSolvable
  
    
  def moves(self):                   # min number of moves to solve initial board; -1 if unsolvable
    return self.moves
  
    
  def solution(self):     # sequence of boards in a shortest solution; null if unsolvable
    if not self.isSolvable:
        return None 
    solution = []
    current = self.goalNode
    while current is not None:
      solution.unshift(current.board)
      current = current.previous
    
    return solution
  
  @classmethod  
  def test(cls, *args): # solve a slider puzzle (given below)
    # create initial board from file
    # In in = new In(args[0]);
    # int n = in.readInt();
    # int[][] blocks = new int[n][n];
    # for (int i = 0; i < n; i++)
    #     for (int j = 0; j < n; j++)
    #         blocks[i][j] = in.readInt();
    # Board initial = new Board(blocks);
    # blocks = [[1, 0], [2, 3]]
    # blocks = [[8, 6, 7], [2, 5, 4], [1, 3, 0]]
    blocks = []
    n = int(args[0].strip())
    # p n
    # blocks = [[8, 6, 7], [2, 5, 4], [3, 0, 1]]
    item = 1
    while (item):
      # break if item.chomp.empty?
      try: item = raw_input().rstrip()
      except EOFError: 
            break
      if not item: break
        
      a = item.rstrip().split(" ")
      a = list(map(int, a))
      blocks.append(a)
    
    # p blocks
    initial =  Board(blocks)

    # solve the puzzle
    solver =  Solver(initial)

    # print solution to standard output
    if not solver.isSolvable():
      print( "No solution possible")
    else: 
        print( "Minimum number of moves =  {}".format(solver.moves()))
        for board in solver.solution():
            print(board)


# p ARGV
# ARGF.each_with_index do |line, idx|
#     print ARGF.filename, ": ", idx, "; ", line
#     puts
#     a = line.split
#     puts a
# end
print(sys.argv)
sys.argv.pop(1)
print(sys.argv)
Solver.test(sys.argv.pop(1))