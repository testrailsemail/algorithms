class Deque
  include Enumerable

  attr_reader :first, :last, :n
  # top of queue, size of the queue

  def initialize()
    @first = nil
    @last = nil
    @n = 0
  end
  
  def empty?
    n == 0
  end
  
  def size                        # return the number of items on the deque
    n
  end
  
  def addFirst(item)              # add the item to the front
    # validate(item)
    temp = @first
    @first = Node.new
    @first.item = item
    @first.next = temp
    @first.previous = nil
    temp.previous = @first if temp != nil
    @n += 1
    @last = @first if size == 1
  end
  
  def addLast(item)           # add the item to the end
    # validate(item)
    temp = @last
    @last = Node.new
    @last.item = item
    @last.next = nil
    @last.previous = temp
    temp.next = @last if temp != nil
    @n += 1
    @first = @last if size == 1
  end

  def removeFirst               # remove and return the item from the front
    canRemove?
    temp = @first
    return removeAll(@first) if size == 1 
    @first = @first.next
    @first.previous = nil
    @n -= 1
    return temp.item
  end
  
  def removeLast               # remove and return the item from the end
    canRemove?
    return removeAll(@last) if size == 1
    temp = @last
    @last = @last.previous
    @last.next = nil
    @n -= 1
    return temp.item
  end
  
  def inspect
    s = []
    each do |node|
        s << node.item
        # s << ' '
    end
    return s.to_s
  end

  def to_s
    s = []
    each do |node|
        s << node.item
        # s << ' '
    end
    return s.to_s
  end

  def to_a
    each.collect {|node| node.item }
  end
  
  # public Iterator<Item> iterator() {        # return an iterator over items in order from front to end
  #   return new ListIterator<Item>(first);
  # }
  
  def each(&block)
    return enum_for(:each) unless block_given?

    current = @first
    while current != nil
      yield(current)
      current = current.next
    end
    
    self
  end
  
  def self.test (*args)   # unit testing (optional)
    stack = Deque.new
    
    stack.addFirst("a")
    stack.addLast("z")
    stack.removeFirst()
    stack.addFirst("b")
# //    StdOut.println("(" + stack + ")");
    stack.removeLast()
    stack.addLast("y")
# //    StdOut.println("(" + stack + ")");
    stack.removeLast()
    stack.removeLast()
    puts "(#{stack.size} left on stack)"
  end
  
  private 

  Node = Struct.new(:item, :next, :previous)
  
  def validate(item)
    raise ArgumentError.new( "You gave me a nil!") if item == nil
  end
  
  def canRemove?()
    raise ArgumentError.new("Deque is empty!") if empty?
  end
  
  def removeAll(item)
    @first = nil
    @last = nil
    @n = 0
    return item.item
  end
end

class RandomizedQueue
  include Enumerable

  attr_accessor :n, :last, :q
#   private Item[] q;       // queue elements
#   private int n;          // number of elements on queue
# //  private int first;      // index of first element of queue
#   private int last;       // index of next available slot
  
  def initialize                 # construct an empty randomized queue
    @q =  [] # Array.new(2)
  end
  
  def empty?                 # is the queue empty?
    @q.empty?
  end
  
  def size                   # return the number of items on the queue
    @q.length
  end
    
  def enqueue(item)           # add the item
    raise ArgumentError.new( "You gave me a nil!") if item == nil
    @q << item
  end
  
  def dequeue                    # remove and return a random item
    raise ArgumentError.new("Queue is empty!") if empty?
    prng = Random.new
    i = prng.rand(size)
    @q.delete_at(i)
  end
    
  def sample                    # return (but do not remove) a random item
    raise ArgumentError.new("Queue is empty!") if empty?
    q.sample
  end
  
  def len
    q.length
  end
  
  def to_s
    q.to_s
  end
  
  def each(&block)
    return enum_for(:each) unless block_given?
    t = q.clone
    t.shuffle!
    t.each(&block)
    self
  end

  def self.test(*args)         # unit testing (optional)
    queue = RandomizedQueue.new
    queue.enqueue("a")
    puts "(#{queue.sample} is in queue)"
    puts "removed:        #{queue.dequeue}"
    queue.enqueue("a")
    queue.enqueue("b")
    queue.enqueue("c")
    queue.enqueue("d")
    queue.each { |i| puts "element:        #{i}" }
    puts "num elements:   #{queue.len}"
    puts "elements:       #{queue}"
    puts "elements:       "
    queue.each { |i| print "#{i} " }
    puts
    puts "array size:     #{queue.len}"
  end
end

class Permutation

  def self.test (*args)

    k = args[0].to_i
    n = 1
    a = Array.new(k)
    prng = Random.new
    
    if (k != 0)
      qu = RandomizedQueue.new
      puts "anything or Enter key to abort"
      while (item = gets) do
        # input = gets  
        # item = input.chomp
        break if item.chomp.empty?
        if (n <= k) 
          a[n-1] = item
          n +=1
        else 
          j = prng.rand(n)
          a[j] = item if (j < k)
          n += 1
        end
      end
      a.each { |i| qu.enqueue(i) }
      qu.each { |i| puts i }
    end
  end
end

# ARGF.each_with_index do |line, idx|
#     print ARGF.filename, ": ", idx, "; ", line
#     puts
#     a = line.split
#     puts a
# end

Permutation.test(ARGV.shift)