require_relative "digraph"
require_relative "BreadthFirstDirectedPaths"
require_relative "MinPQ"
class SAP
  # private final Digraph graph;
  # private final Map<String, SAPProcessor> cache;

  # constructor takes a digraph (not necessarily a DAG)
  def initialize(g)
    @cache = {}
    @graph =  Digraph.from_graph(g)
  end

  # length of shortest ancestral path between v and w; -1 if no such path
  def length(v, w)
    raise ArgumentError.new if !valid_index(v) || !valid_index(w)
    cachedResult(v, w).distance
  end

  # a common ancestor of v and w that participates in a shortest ancestral path; -1 if no such path
  def ancestor(v, w)
    raise ArgumentError.new if !valid_index(v) || !valid_index(w)
    cachedResult(v, w).ancestor
  end

  # length of shortest ancestral path between any vertex in v and any vertex in w; -1 if no such path
  def lengths(v, w) # Iterable<Integer>
    raise ArgumentError.new if !valid_indeces(v) || !valid_indeces(w)
    cachedResult(v, w).distance
  end

  # a common ancestor that participates in shortest ancestral path; -1 if no such path
  def ancestors(v, w) # Iterable<Integer>
    raise ArgumentError.new if !valid_indeces(v) || !valid_indeces(w)
    cachedResult(v, w).ancestor
  end

  # do unit testing of this class
  def self.main(args)
    inp = File.new(args[0].to_s)
    g = Digraph.from_io(inp)
    sap = SAP.new(g)
    # while (gets.chomp != "q") do
      v = args[1].to_i#gets.chomp.to_i
      w = args[2].to_i#gets.chomp.to_i
      length = sap.length(v, w)
      ancestor = sap.ancestor(v, w)
      puts "length = #{length}, ancestor = #{ancestor}"
    # end
  end

  private
  def valid_index(i)
    return false if (i == nil || i < 0 || i >= @graph.v)
    true
  end

  def valid_indeces(vertices)
    return false if vertices == nil
    vertices.each { |vertex| return false if !valid_index(vertex) }
    true
  end

  def cachedResult(v, w)
    key = v.to_s + "_" + w.to_s
    return @cache.delete(key) if @cache.has_key?(key)
    # we need to cache only for 2 consecutive calls (distance and path), therefore we delete the result after the second call

    p = SAPProcessor.new(@graph, v, w)
    @cache[key] = p
  end

  class SAPProcessor
    attr_reader :ancestor, :distance

    def initialize(graph, v, w)
      # .respond_to? :each
      a =  BreadthFirstDirectedPaths.new(graph, v)
      b =  BreadthFirstDirectedPaths.new(graph, w)

      process(graph, a, b)
    end

    private
    def process(graph, a, b)
      ancestors =  MinPQ.new
      shortestAncestor = {}
      (0...graph.v).each do |i|
        if (a.hasPathTo?(i) && b.hasPathTo?(i))
          dist = a.distTo(i) + b.distTo(i)
          ancestors.insert(dist)
          shortestAncestor[dist] = i
        end
      end

      if (ancestors.isEmpty?)
        @distance = -1
        @ancestor = -1

      else
        @distance = ancestors.min
        @ancestor = shortestAncestor[distance]
      end

    end
  end
end

# SAP.main(ARGV)
