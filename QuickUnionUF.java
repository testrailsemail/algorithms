package UF;

public class QuickUnionUF {
	private int[] id;
	private int[] sz; // weighted lgN union and find
	
	public QuickUnionUF(int N) {
		id = new int[N];
		for (int i = 0; i < N; i++) { 
		  id[i] = i;
		  sz[i] = 1; }
	}
	
	private int root(int i) {
		while (i != id[i]) {
			id[i] = id[id[i]];
			i = id[i]; // path compression, flatten the tree
			
		}
		return i;
	}
	
	public boolean connected(int p, int q) {
		return root(p) == root(q);
	}
	
	public void union(int p, int q) {
//		id[root(p)] = root(q);
		int i = root(p);
		int j = root(q);
		if (i == j) return; // weighted
		if (sz[i] < sz[j]) { // weighted
			id[i] = j; 
			sz[j] += sz[i]; // weighted
			}
		else // weighted
			{ 
			id[j] = i; 
			sz[i] += sz[j]; 
			}
		
	}
}
