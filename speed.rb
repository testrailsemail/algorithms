require 'benchmark'
require "memory_profiler"
require_relative 'queue_fast'
require_relative 'queueSlow'

GC.disable


number_of_elements = 3_600_000

a1 =[]
a2 = []
a3 = []
a4 = []
q1 = Queue.new
q2 = Queue.new
# q3 = Que.new
# q4 = Que.new
# q5 = QueFast.new
# q6 = QueFast.new



puts number_of_elements


# # MEMORY**************************************************************

# report = MemoryProfiler.report do
#   q1 = Queue.new
#   number_of_elements.times do
#     q1.enq(true)
#   end
# end
# report.pretty_print

# q1 = 1

# report = MemoryProfiler.report do
#   q3 = Que.new
#   number_of_elements.times do
#     q3.enqueue(true)
#   end
# end
# report.pretty_print

# q3 = 1

# report = MemoryProfiler.report do
#   q5 = QueFast.new
#   number_of_elements.times do
#     q5.enqueue(true)
#   end
# end
# report.pretty_print

# q5 = 1


number_of_elements.times do
  q1.enq(true)
  q2.enq(true)
  # q3.enqueue(true)
  # q4.enqueue(true)
  # q5.enqueue(true)
  # q6.enqueue(true)
  a1 << true
  a2 << true
  a3 << true
  a4 << true
end

number_of_operations = 1000

Benchmark.bm do |bm|
  puts "Queue#enq('test')"
  bm.report do
    number_of_operations.times { q1.enq('test') }
  end

  puts "Queue#deq"
  bm.report do
    number_of_operations.times { q2.deq }
  end

  # puts "Que#enq('test')"
  # bm.report do
  #   number_of_operations.times { q3.enqueue('test') }
  # end

  # puts "Que#deq('test')"
  # bm.report do
  #   number_of_operations.times { q4.dequeue }
  # end

  # puts "QueFast#enq('test')"
  # bm.report do
  #   number_of_operations.times { q5.enqueue('test') }
  # end

  # puts "QueFast#deq('test')"
  # bm.report do
  #   number_of_operations.times { q6.dequeue }
  # end

  puts "Array#shift"
  bm.report do
    number_of_operations.times { a1.shift }
  end

  puts "Array#unshift"
  bm.report do
    number_of_operations.times { a2.unshift('test') }
  end

  puts "Array#pop"
  bm.report do
    number_of_operations.times { a3.pop }
  end

  puts "Array#<<"
  bm.report do
    number_of_operations.times { a4 << 'test' }
  end
end

