import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

// import java.util.ArrayList;
// import java.util.Arrays;
// import java.util.List;

// import edu.princeton.cs.algs4.QuickFindUF;

public class Percolation {
  private int[][] site;
  private int high;
  private int size;
  private int filled;
  private int toFill;
  private int opened;
  private WeightedQuickUnionUF id;
  
  public Percolation(int n) {               // create n-by-n grid, with all sites blocked
  
    if (n <= 0) {
      throw new java.lang.IllegalArgumentException("n must be a number bigger than 0");
    }
    site = new int[n + 1][n + 1];
    for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= n; j++) {
        site[i][j] = 0;
      }
    }
    high = n;
    size = n * n + 2;
    filled = size - 2;
    toFill = size - 1;
        
    id = new WeightedQuickUnionUF(size);
    opened = 0;
  }
  
  private void except(int row, int col)
  {
    boolean forRow = row <= 0 || row > high;
    boolean forCol = col <= 0 || col > high;
    if (forRow || forCol) throw new java.lang.IndexOutOfBoundsException();
  } 
  
  private int trans(int row, int col) {
    return high * (row - 1) + (col - 1) % high;
  }
//    private List<Integer> transB(int n)
//    {
//  int i = n/high + 1;
//      int j = n%high+1;
//      return {i,j};
//  }
  
  private int[] neighbors(int row, int col) {
    int i = 0;
    int[] neighbor = new int[4];
    boolean corner = (row == 1 || row == high) && (col == 1 || col == high);
    boolean side = (row == 1 || row == high || col == 1 || col == high);

    for (int y = Math.max(1, col - 1); y <= Math.min(col + 1, high); y++) {
      if (y != col) {
        neighbor[i] = trans(row, y); //.add( trans(row,y) );
        i++;
      }
    }
        for (int x = Math.max(1, row-1); x <= Math.min(row+1, high); x++) {
            if (x != row) {
                neighbor[i] = trans(x, col); //.add( trans(x, col) );
                i++;
            }
        }
        if (corner) { 
            neighbor[2] = -1; 
            neighbor[3] = -1; 
        }
        else if (side) { neighbor[3] = -1; }
        else { 
            
        }
//        System.out.println(Arrays.toString(neighbor.toArray()));
        return neighbor;
    }
    public    void open(int row, int col)    // open site (row, col) if it is not open already
    {
        except(row, col);
        if (!isOpen(row, col)) {
          int p = trans(row, col);
//  System.out.println("opened id: " + p);
          if (row == high) { id.union(p, toFill); }
          if (row == 1) { id.union(p, filled); }
          else {
            for (int n : neighbors(row, col)) {
              if (n != -1) {
                if (id.connected(n, filled)) {
                  id.union(p, n);
                  break;
                }
                if (isOpen(n/high + 1, n % high+1)) {
                  id.union(p, n);
                }
              }
            }
          }
          if (isFull(row, col)) {
            for (int n : neighbors(row, col)) {
              if (n != -1 && isOpen(n/high + 1, n % high+1) && !id.connected(n, filled)) {
                id.union(n, p);
              }
            }
          }
          opened += 1;
          site[row][col] = 1;
        }

    }
    public boolean isOpen(int row, int col)  // is site (row, col) open?
    {
        except(row, col);
        return site[row][col] == 1;
    }
    public boolean isFull(int row, int col)  // is site (row, col) full?
    {
        except(row, col);
        int p = trans(row, col);
        if (id.connected(p, filled)) return true;
        return false;
    }
    public     int numberOfOpenSites()       // number of open sites
    { return opened; }
    public boolean percolates()             // does the system percolate?
    {
//        for (int j = 1; j <= high; j++) {
//            if (isFull(high, j)) return true; 
//        }
      if (id.connected(toFill, filled)) return true;
      return false;
    }

    public static void main(String[] args)   // test client (optional)
    {

      int n = 5;
//      int coln = 5;
      Percolation start = new Percolation(n);
//  for (int i = 1; i <=5; i++) {
//  System.out.println("row: " + i);
//      start.open(i, coln);
//    System.out.println("full: " + start.isFull(i, coln));
//  System.out.println("percolates: " + Boolean.toString(start.percolates()) );
//      }

      while (!start.percolates()) {
        int row = StdRandom.uniform(1, n+1);
        int col = StdRandom.uniform(1, n+1);

        start.open(row, col);
        System.out.println("Opened sites: " + start.numberOfOpenSites());
// System.out.println("percolates: " + start.percolates());
      } 
      System.out.println("End: " + Boolean.toString(start.percolates()) + " sites " + start.numberOfOpenSites()); 
//  System.out.println(Arrays.deepToString(start.site));
    }
}