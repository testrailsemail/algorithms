import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class MyStack<Item> {
  private Node first = null;
  private int n;
  
  
  private class Node {
    private Item item;
    private Node next;
  }
  
  public MyStack() {
    first = null;
    n = 0;
  }
  
  public void push(Item item) {
    Node oldfirst = first;
    first = new Node();
    first.item = item;
    first.next = oldfirst;
    n++;
  }
  
  public Item pop() {
    Item item = first.item;
    first = first.next;
    n--;
    return item;
  }
  
  public boolean isEmpty() {
    return first == null;
  }
  
  public int size() {
    return n;
  }
  
  public static void main(String[] args) {
    MyStack<String> stack = new MyStack<String>();
    while (!StdIn.isEmpty()) {
      String s = StdIn.readString();
      if (s.equals("-")) StdOut.print(stack.pop());
      else stack.push(s);
    }

  }

}
