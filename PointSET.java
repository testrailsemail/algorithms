import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.SET;
import edu.princeton.cs.algs4.StdDraw;

public class PointSET {
  private SET<Point2D> points;
  
   public         PointSET() {                               // construct an empty set of points
     points = new SET<Point2D>();
   }
   
   public           boolean isEmpty() {                     // is the set empty?
     return points.isEmpty();
   }
   
   public               int size() {                        // number of points in the set
     return points.size();
   }
   
   public              void insert(Point2D p) {             // add the point to the set (if it is not already in the set)
     if (p == null)
       throw new NullPointerException("The point is null");
     points.add(p);
   }
   
   public           boolean contains(Point2D p) {           // does the set contain point p?
     if (p == null)
       throw new NullPointerException("The point is null");
     return points.contains(p);
   }
   
   public              void draw() {                        // draw all points to standard draw
     if (isEmpty()) return;
     StdDraw.setPenRadius(0.1);
     StdDraw.setPenColor(StdDraw.BLACK);
     for (Point2D point : points) {
       point.draw();
     }
   }
   
   public Iterable<Point2D> range(RectHV rect) {            // all points that are inside the rectangle
     if (rect == null)
       throw new NullPointerException();
//     if (isEmpty())
//       return null;
     Queue<Point2D> containe = new Queue<Point2D>();
     for (Point2D point : points) {
       if (rect.contains(point))
         containe.enqueue(point);
     }
//     if (containe.isEmpty())
//       return null;
     
     return containe;
   }
   
   public Point2D nearest(Point2D p) {            // a nearest neighbor in the set to point p; null if the set is empty
     if (p == null)
       throw new NullPointerException();
     if (isEmpty())
       return null;
     Point2D nearest = null;
     double min = Double.MAX_VALUE;
     for (Point2D point : points) {
       if (point.distanceSquaredTo(p) < min) {
         min = point.distanceSquaredTo(p);
         nearest = point;
       }
     }
     return nearest;
   }

   public static void main(String[] args) {                 // unit testing of the methods (optional)
     
   }
}