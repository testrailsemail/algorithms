require_relative 'WordNet'
# require_relative "digraph"

class Outcast
  def initialize(wordnet)         # constructor takes a WordNet object
    @net = wordnet
  end

  def outcast(nouns) # given an array of WordNet nouns, return an outcast
    maxDistance = -1
    outcast = nil

    nouns.each do |noun1|
      distance = 0
      nouns.each do |noun2|
        unless noun1 == noun2
          distance += @net.distance(noun1, noun2)
        end
      end
      if (distance > maxDistance)
        maxDistance = distance
        outcast = noun1
      end
    end

    outcast
  end

  def self.main(args) # see test client below
    wordnet = WordNet.new(args[0], args[1])
    outcast = Outcast.new(wordnet)
    args[2..-1].each do |arg|
    # for (int t = 2; t < args.length; t++) {
      inp = File.new(arg.to_s)
      nouns = inp.readlines.map { |e| e.chomp.to_s }.select { |e| e.length != 0 }
      puts "#{arg}: #{outcast.outcast(nouns)}"
    end
  end
end

# Outcast.main(ARGV)
