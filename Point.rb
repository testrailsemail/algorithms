class Point
  include Comparable
  attr_reader :x, :y

  def initialize(x, y)
      @x = x
      @y = y
  end


    #  * Returns the slope between this point and the specified point.
    #  * Formally, if the two points are (x0, y0) and (x1, y1), then the slope
    #  * is (y1 - y0) / (x1 - x0). For completeness, the slope is defined to be
    #  * +0.0 if the line segment connecting the two points is horizontal;
    #  * Double.POSITIVE_INFINITY if the line segment is vertical;
    #  * and Double.NEGATIVE_INFINITY if (x0, y0) and (x1, y1) are equal.

  def slopeTo(that)
    # (that.x == x && that.y == y) ? -1.0/0 : (that.x == @x ? 1.0/0 : (that.y == @y ? +0.0 : (that.y.to_f - @y.to_f)/(that.x - @x)))
    if that.x == x && that.y == y 
      then return -1.0/0
    elsif (that.x == @x) then return 1.0/0
    elsif (that.y == @y) then return +0.0
    else tg = (that.y.to_f - @y.to_f)/(that.x - @x)
    end
    return tg
  end

  def slopeOrder(arr)
    arr.sort_by { |point| [self.slopeTo(point), point]}
  end
  
# a.sort! {|m,n| (slopeTo([0, 1], m) <=> slopeTo([0, 1], n)) == 0 ? ((m[1] <=> n[1]) == 0 ? m[0] <=> n[0] : m[1] <=> n[1]) : slopeTo([0, 1], m) <=> slopeTo([0, 1], n) }

  #  * Compares two points by y-coordinate, breaking ties by x-coordinate.
  #  * Formally, the invoking point (x0, y0) is less than the argument point
  #  * (x1, y1) if and only if either y0 < y1 or if y0 = y1 and x0 < x1.

  def <=>(that)
    # raise ArgumentError.new( "You gave me a nil!") if that == nil
    (@y <=> that.y) == 0 ? @x <=> that.x : @y <=> that.y
  end

   # /**
   # * Returns a string representation of this point.
   # * This method is provide for debugging;
   # * your program should not rely on the format of the string representation.
   # *
   # * @return a string representation of this point
   # */
  def to_s
    "(" + @x.to_s + ", " + @y.to_s + ")"
  end   

    # /**
    #  * Unit tests the Point data type.
    #  */
  def self.test(*args)
    x = Point.new(1, 2)
    x1 = Point.new(2, 1)
    y = Point.new(1, 3)
    z = Point.new(-1, -2)
    a = Point.new(-1, 2)
    b = Point.new(4, 2)
    c = Point.new(1, 4)
    poi = [] 
    (0..6).each {|i| poi[i] = Point.new(i, i)}
    points = [a, y, c, x1, z, b, x, poi[0], poi[1], poi[2], poi[3], poi[4]]
    puts "x:    #{x};  y: #{y}; a: #{a}"
    puts "x, y:  #{x<=>y}, x, a:   #{x<=>a}"
    puts "Points unsorted: "
    points.each {|point| print " #{point}"}
    puts
    points.sort!
    puts "Points sorted: "
    points.each {|point| print " #{point}"}
    puts
    points.sort_by! { |point| [x1.slopeTo(point), point]}
    puts "Points sorted by slope to x1:"
    points.each {|point| print " #{point}"}
    puts
# # x.draw();
# # LineSegment line = new LineSegment(x, a);
# # line.draw();
# # Point[] poin = {a, y, c, x1, z, b, x, poi[0], poi[1], poi[2], poi[3], poi[4], poi[5], poi[6]};
# # BruteCollinearPoints p = new BruteCollinearPoints(poin);
# # StdOut.println();
# # StdOut.println("Segments:");
# # System.out.println(p.numberOfSegments());
# # for (LineSegment segment : p.segments()) {
# # StdOut.println(" " + segment + "; ");
# # }
# FastCollinearPoints t = new FastCollinearPoints(poin);
# StdOut.println(t.numberOfSegments());
 # read the n points from a file
 #      In in = new In(args[0]);
 #      int n = in.readInt();
 #      Point[] points = new Point[n];
 #      for (int i = 0; i < n; i++) {
 #          int x = in.readInt();
 #          int y = in.readInt();
 #          points[i] = new Point(x, y);
 #      }

 #      // draw the points
 #      StdDraw.enableDoubleBuffering();
 #      StdDraw.setXscale(0, 32768);
 #      StdDraw.setYscale(0, 32768);
 #      for (Point p : points) {
 #          p.draw();
 #      }
 #      StdDraw.show();

 #      // print and draw the line segments
 #      FastCollinearPoints collinear = new FastCollinearPoints(points);
 #      for (LineSegment segment : collinear.segments()) {
 #          StdOut.println(segment);
 #          segment.draw();
 #      }
 #      StdDraw.show();
  end
end

class LineSegment
  # /**
  #  * Initializes a new line segment.
  #  *
  #  * @param  p one endpoint
  #  * @param  q the other endpoint
  #  * @throws NullPointerException if either <tt>p</tt> or <tt>q</tt>
  #  *         is <tt>null</tt>
  #  */
  def initialize(p, q)
    raise ArgumentError.new( "You gave me a nil!") if (p.nil? || q.nil?)
    @p = p
    @q = q
  end
   # * Returns a string representation of this line segment
   # * This method is provide for debugging;
   # * your program should not rely on the format of the string representation.
  def to_s
    "#{p} -> #{q}"
  end
end

class FastCollinearPoints
# //  private int segments;
  
  def initialize(points)   # finds all line segments containing 4 or more points
    raise ArgumentError.new( "You gave me a nil!") if points.nil?
    raise ArgumentError.new("one of the points is null") if points.any?{ |e| e.nil? }
    raise ArgumentError.new("one of the points is repeated") if points.detect{ |e| points.count(e) > 1 }
    
    lines = {}
    n = points.length
    if (n < 4) 
      @segmentsList = []
      return
    end
       
    pointsC = points.clone
    
    points.each do |i|
      pointsC.sort_by! { |p| [i.slopeTo(p), p] }
      t = 2
      counter = 2;
      j = 1
      
      slope = pointsC[0].slopeTo(pointsC[j])
       
      while (t < n) do
        if (slope == pointsC[0].slopeTo(pointsC[t])) 
          counter += 1
        else 
          if counter >= 4
            low = i > pointsC[j] ? pointsC[j] : i
            high = i >= pointsC[t-1] ? i : pointsC[t-1]
            line =  LineSegment.new(low, high)
            key = low.to_s + high.to_s
            lines[key] = line
          end
          j = t
          counter = 2
          slope = pointsC[0].slopeTo(pointsC[j])
        end
        t +=1
      end
      if counter >= 4
            low = i > pointsC[j] ? pointsC[j] : i
            high = i >= pointsC[t-1] ? i : pointsC[t-1]
            line =  LineSegment.new(low, high)
            key = low.to_s + high.to_s
            lines[key] = line
      end

    end
    @segmentsList = lines.values

  end
  def  numberOfSegments # the number of line segments
    @segmentsList.length
  end

  def segments               # the line segments
    @segmentsList.clone
  end
end