require 'benchmark'
# require 'ostruct'
class Node
  attr_accessor :next_item, :item
  def initialize(item = nil, next_node = nil)
    @item = item
    @next_node = next_item
  end
end


class Stack
  def initialize
    @first = nil
  end

  def push(item)
    @first = Node.new(item, @first)
  end
  alias_method :"<<", :push

  def pop
    raise "Stack is empty" if empty?
    item = @first.item
    @first = @first.next_node
    item
  end

  def empty?
    @first.nil?
  end
end


number_of_operations = 100000

Benchmark.bm do |bm|
  puts "Queue#enq('test')"
  bm.report do
    number_of_operations.times { q1.enq('test') }
  end

  puts "Queue#deq"
  bm.report do
    number_of_operations.times { q2.deq }
  end

  puts "Que#enq('test')"
  bm.report do
    number_of_operations.times { q3.enqueue('test') }
  end

REP = 100000

User = Struct.new(:name, :age)

USER = "User".freeze
AGE = 21
HASH = {:name => USER, :age => AGE}.freeze

Benchmark.bm 20 do |x|
  x.report 'Node slow' do
    REP.times do |index|
      a=Node.new("User")
      a.next_item = 21
    end
  end

  x.report 'Node fast' do
    REP.times do |index|
       Node.new("User", 21)
    end
  end

  x.report 'Struct slow' do
    REP.times do |index|
      User.new("User", 21)

    end
  end

  x.report 'Struct fast' do
    REP.times do |index|
      a= User.new(USER)
      a.age= AGE
    end
  end
end
require 'benchmark/ips'

# Benchmark.bm do |x|
#   x.report('Array#unshift') do
#     array = []
#     100_000.times { |i| array.unshift(i) }
#   end

#   x.report('Array#insert') do
#     array = []
#     100_000.times { |i| array.insert(0, i) }
#   end

#   x.compare!
# end
