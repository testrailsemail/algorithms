import java.util.TreeSet;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.StdOut;

public class BoggleSolver {
  
  // Idea from i-net to use course's TrieST with only capital letters
  private static class Node {
      private Object val;
      private Node[] next = new Node[26];
      
      public Node next(char c) {
        return next[c - 65];
      }
    }
  private static class BoggleTrieST<Value> {
    private static final int R = 26; // A-Z letters
    private static final int OFFSET = 65; // Offset of letter A in ASCII table

    private Node root;
    private int n;          // number of keys in trie

    // R-way trie node
    

    /**
     * Initializes an empty string symbol table.
     */
    public BoggleTrieST() {
    }

    /**
     * Does this symbol table contain the given key?
     * @param key the key
     * @return {@code true} if this symbol table contains {@code key} and
     *     {@code false} otherwise
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public boolean contains(String key) {
      if (key == null) throw new IllegalArgumentException("argument to contains() is null");
      return get(key) != null;
    }

    public Value get(String key) {
      if (key == null) throw new IllegalArgumentException("argument to get() is null");
      Node x = get(root, key, 0);
      if (x == null) return null;
      return (Value) x.val;
    }
    
    public Node root() {
      return root;
    }

    private Node get(Node x, String key, int d) {
      if (x == null)  return null;
      if (d == key.length())  return x;
      char c = key.charAt(d);
      return get(x.next[c - OFFSET], key, d+1);
    }

    /**
     * Inserts the key-value pair into the symbol table, overwriting the old value
     * with the new value if the key is already in the symbol table.
     * If the value is {@code null}, this effectively deletes the key from the symbol table.
     * @param key the key
     * @param val the value
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public void put(String key, Value val) {
      if (key == null) throw new IllegalArgumentException("first argument to put() is null");
      if (val == null) delete(key);
      root = put(root, key, val, 0);
    }

    private Node put(Node x, String key, Value val, int d) {
      if (x == null)  x = new Node();
      if (d == key.length()) {
        if (x.val == null) n++;
        x.val = val;
        return x;
      }
      char c = key.charAt(d);
      x.next[c - OFFSET] = put(x.next[c - OFFSET], key, val, d+1);
      return x;
    }
    
    /**
     * Returns the number of key-value pairs in this symbol table.
     * @return the number of key-value pairs in this symbol table
     */
    public int size() {
      return n;
    }
    
    /**
     * Is this symbol table empty?
     * @return {@code true} if this symbol table is empty and {@code false} otherwise
     */
    public boolean isEmpty() {
      return size() == 0;
    }

    /**
     * Returns the string in the symbol table that is the longest prefix of {@code query},
     * or {@code null}, if no such string.
     * @param query the query string
     * @return the string in the symbol table that is the longest prefix of {@code query},
     *     or {@code null} if no such string
     * @throws IllegalArgumentException if {@code query} is {@code null}
     */
    public String longestPrefixOf(String query) {
      if (query == null) throw new IllegalArgumentException("argument to longestPrefixOf() is null");
      int length = longestPrefixOf(root, query, 0, -1);
      if (length == -1) return null;
      return query.substring(0, length);
    }

    /**
     * Returns the string in the symbol table that is the longest prefix of {@code query},
     * or {@code null}, if no such string.
     * @param query the query string
     * @return the string in the symbol table that is the longest prefix of {@code query},
     *     or {@code null} if no such string
     * @throws IllegalArgumentException if {@code query} is {@code null}
     */
    private int longestPrefixOf(Node x, String query, int d, int length) {
      if (x == null)
        return length;
      if (x.val != null)
        length = d;
      if (d == query.length())
        return length;
      char c = query.charAt(d);
      return longestPrefixOf(x.next[c - OFFSET], query, d + 1, length);
    }

    public Iterable<String> keys() {
      return keysWithPrefix("");
    }

    public Iterable<String> keysWithPrefix(String prefix) {
      Queue<String> results = new Queue<String>();
      Node x = get(root, prefix, 0);
      collect(x, new StringBuilder(prefix), results);
      return results;
    }

    public boolean isPrefix(String prefix) {
      return get(root, prefix, 0) != null;
    }

//    public NodeType getNodeType(String key) {
//      Node x = get(root, key, 0);
//      if (x == null)
//        return NodeType.NON_MATCH;
//      else if (x.val == null)
//        return NodeType.PREFIX;
//      else
//        return NodeType.MATCH;
//    }

    private void collect(Node x, StringBuilder prefix, Queue<String> results) {
      if (x == null)
        return;
      if (x.val != null)
        results.enqueue(prefix.toString());
      for (int c = 0; c < R; c++) {
        prefix.append(c);
        collect(x.next[c - OFFSET], prefix, results);
        prefix.deleteCharAt(prefix.length() - 1);
      }  
    }

    public Iterable<String> keysThatMatch(String pattern) {
      Queue<String> results = new Queue<String>();
      collect(root, new StringBuilder(), pattern, results);
      return results;
    }

    public void collect(Node x, StringBuilder prefix, String pattern, Queue<String> results) {
      if (x == null)  return;
      int d = prefix.length();
      if (prefix.length() == pattern.length() && x.val != null)
        results.enqueue(prefix.toString());
      if (d == pattern.length())
        return;
      char c = pattern.charAt(d);
      if (c == '.') {
        for (char ch = 0; ch < R; ch++) {
            prefix.append(ch);
            collect(x.next[ch - OFFSET], prefix, pattern, results);
            prefix.deleteCharAt(prefix.length() - 1);
        }
      }
      else {
          prefix.append(c);
          collect(x.next[c - OFFSET], prefix, pattern, results);
          prefix.deleteCharAt(prefix.length() - 1);
      }
    }
    
    public void delete(String key) {
      if (key == null) throw new IllegalArgumentException("argument to delete() is null");
      root = delete(root, key, 0);
    }

    private Node delete(Node x, String key, int d) {
      if (x == null)  return null;
      if (d == key.length()) {
        if (x.val != null) n--;
        x.val = null;
      }
      else {
        char c = key.charAt(d);
        x.next[c - OFFSET] = delete(x.next[c - OFFSET], key, d+1);
      }
      // remove subtrie rooted at x if it is completely empty
      if (x.val != null) return x;
      for (int c = 0; c < R; c++)
        if (x.next[c - OFFSET] != null)
          return x;
      return null;
    }
  }
  
  private BoggleTrieST<Integer> dict = new BoggleTrieST<>();
  private boolean[] marked;
  // Initializes the data structure using the given array of strings as the dictionary.
  // (You can assume each word in the dictionary contains only the uppercase letters A through Z.)
  public BoggleSolver(String[] dictionary) {
    for (String s : dictionary) {
      dict.put(s, 1);
    }
  }

  // Returns the set of all valid words in the given Boggle board, as an Iterable.
  public Iterable<String> getAllValidWords(BoggleBoard board) {
    TreeSet<String> words = new TreeSet<>();
    char[] letters = new char[board.cols() * board.rows()];
    int cols = board.cols();
    int rows = board.rows();
    for (int i = 0; i < rows; i++) 
      for (int j = 0; j < cols; j++) {
        letters[i*cols + j] = board.getLetter(i, j);
      }
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < cols; j++) {
        searchForWords(letters, cols, i, j, words);
      }
    }
    return words;
  }
  
  private void searchForWords(char[] board, int cols, int i, int j, TreeSet<String> words) {
    marked = new boolean[board.length]; // new boolean[board.rows()][board.cols()];
    Node root = dict.root();
    dfs(board, cols, i, j, words, new StringBuilder(), root);
  }
  
  private void dfs(char[] board, int cols, int i, int j, TreeSet<String> words, StringBuilder prefix, Node node) {
    int pos = cols*i + j;
    if (marked[pos]) return;
    char letter = board[pos]; // .getLetter(i, j);
    
//    prefix = prefix + (letter == 'Q' ? "QU" : letter);
//    int d = Math.max(0, prefix.length()-1);
//    StdOut.println("prefix: " + letter);
    Node x = node.next(letter); // dict.get(node, prefix, d);
    if (letter == 'Q') {
      if (x != null)
        x = x.next('U'); // "U".charAt(0));
    }
//    if (!dict.isPrefix(prefix)) return;
    if (x == null) return;
    prefix = prefix.append(letter == 'Q' ? "QU" : letter);
    int length = prefix.length();
//    if (prefix.equals("EQUA"))
//      prefix = prefix;
//    if (prefix.length() >= 3 && dict.contains(prefix)) words.add(prefix);
    if (length >= 3 && x.val != null) words.add(prefix.toString());
//    StdOut.println("prefix: " + prefix);
    marked[pos] = true;
    for (int k = Math.max(0, i-1); k <= Math.min(board.length/cols - 1, i+1); k++)
      for (int l = Math.max(0, j-1); l <= Math.min(cols-1, j+1); l++)
        if (k == i && l == j)
          continue;
        else
          dfs(board, cols, k, l, words, prefix, x);
    // key point: release the letter as it not used for searching sequence 
    marked[pos] = false;
    prefix.delete(length - (letter == 'Q' ? 2 : 1), length);
  }

  // Returns the score of the given word if it is in the dictionary, zero otherwise.
  // (You can assume the word contains only the uppercase letters A through Z.)
  public int scoreOf(String word) {
    if (dict.contains(word)) {
      if (word.length() < 3) {
        return 0;
      } else if (word.length() < 5) {
        return 1;
      } else if (word.length() < 6) {
        return 2;
      } else if (word.length() < 7) {
        return 3;
      } else if (word.length() < 8) {
        return 5;
      } else {
        return 11;
      }
  }
  return 0;
  }
  
  public static void main(String[] args) {
    In in = new In(args[0]);
    String[] dictionary = in.readAllStrings();
    BoggleSolver solver = new BoggleSolver(dictionary);
    BoggleBoard board = new BoggleBoard(args[1]);
    int score = 0;
    for (String word : solver.getAllValidWords(board)) {
      StdOut.println(word);
      score += solver.scoreOf(word);
    }
    StdOut.println("Score = " + score);
  }
}
