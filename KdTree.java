import java.util.Comparator;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

public class KdTree {
  private static class Node { 
    private Point2D point;
    private Node lt;
    private Node gt;
    private RectHV rect;
    
    
    private Node(Point2D p) {
      point = p;
    }

//    public int compareTo(Node that) {
//      if (that == null)
//        throw new NullPointerException("one of the nodes is null");
//      if (this.point.x() < that.point.x()) return -1;
//      else if (this.point.x() == that.point.x())
//        if (this.point.y() == that.point.y())
//          return 0;
//        else if (this.point.y() < that.point.x())
//          return -1;
//        else return 1;
//      return 1;
//    }
  }
  private Node root;
  private int n;
  
  public         KdTree() {                               // construct an empty set of points
  }
   
   public           boolean isEmpty() {                     // is the set empty?
     return n == 0;
   }
   
   public               int size() {                        // number of points in the set
     return n;
   }
   
   public              void insert(Point2D p) {             // add the point to the set (if it is not already in the set)
     if (p == null) throw new NullPointerException();
     insert(root, p, 0);
   }
   
   private Node insert(Node node, Point2D p, int level) { // , RectHV rect
     if (node == null) {
       Node no = new Node(p);
       // no.rect = rect;
//       StdOut.println(p);
//       StdOut.println(level);
//       StdOut.println(no.rect);
       if (isEmpty()) {
         root = no;
         root.rect = new RectHV(0, 0, 1, 1);
       }
       n++;
       return no;
     }
     if (node.point.equals(p)) return node;
     if (compare(node, p, level) < 0) {
       level++;
       node.lt = insert(node.lt, p, level);
       if (node.lt.rect == null) {
         if (level % 2 == 0)
           node.lt.rect = new RectHV(node.rect.xmin(), node.rect.ymin(), node.rect.xmax(), node.point.y());
         else
           node.lt.rect = new RectHV(node.rect.xmin(), node.rect.ymin(), node.point.x(), node.rect.ymax());
       }
     }
     else { // if (compare(node, p, level) > 0) {
       level++;
       node.gt = insert(node.gt, p, level);
       if (node.gt.rect == null) {
         if (level % 2 == 0)
           node.gt.rect = new RectHV(node.rect.xmin(), node.point.y(), node.rect.xmax(), node.rect.ymax());
         else
           node.gt.rect = new RectHV(node.point.x(), node.rect.ymin(), node.rect.xmax(), node.rect.ymax());
       }
     }
     
     return node;
   }
   
   private int compare(Node node, Point2D p, int level) {
     Comparator<Point2D> cmp;
     if (level % 2 == 0) cmp = Point2D.X_ORDER;
     else cmp = Point2D.Y_ORDER;
     return cmp.compare(p, node.point);
   }
   
   public           boolean contains(Point2D p) {           // does the set contain point p?
     if (p == null)
       throw new NullPointerException("The point is null");
     return contains(root, p, 0);
   }
   
   private boolean contains(Node node, Point2D p, int level) {
     if (node == null) return false;
     int lev = level;
     lev++;
     if (node.point.equals(p)) return true;
     int cmp = compare(node, p, level);
     if (cmp < 0)
       return contains(node.lt, p, lev);
     else 
       return contains(node.gt, p, lev);
   }
   
   public              void draw() {                        // draw all points to standard draw
     if (isEmpty()) return;
     draw(root, 0);
   }
   
   private void draw(Node node, int level) {
     if (node == null) return;
     
     StdDraw.setPenRadius(0.01);
     StdDraw.setPenColor(StdDraw.BLACK);
     node.point.draw();
//     StdOut.println(level);
     StdDraw.setPenRadius(0.001);
     if (level % 2 == 0) {
       StdDraw.setPenColor(StdDraw.RED);
       StdDraw.line(node.point.x(), node.rect.ymin(), node.point.x(), node.rect.ymax());
     }
     else {
       StdDraw.setPenColor(StdDraw.BLUE);
       StdDraw.line(node.rect.xmin(), node.point.y(), node.rect.xmax(), node.point.y());
     }
     level++;
     int lev = level;
     
     draw(node.lt, lev);
     draw(node.gt, lev);  
   }
   
   public Iterable<Point2D> range(RectHV rect) {            // all points that are inside the rectangle
     if (rect == null)
       throw new NullPointerException();
     Queue<Point2D> containe = new Queue<Point2D>();
     range(root, rect, containe);
//     if (containe.isEmpty())
//       return null;
     return containe;
   }
   
   private void range(Node node, RectHV rect, Queue<Point2D> queue) {
     if (node == null) return;
     if (!rect.intersects(node.rect)) return;
     else {
       range(node.lt, rect, queue);
       if (rect.contains(node.point)) queue.enqueue(node.point);
       range(node.gt, rect, queue);
     }
   }
   
   public Point2D nearest(Point2D p) {            // a nearest neighbor in the set to point p; null if the set is empty
     if (p == null)
       throw new NullPointerException();
     if (isEmpty())
       return null;
     Point2D nearest = null;
     double min = Double.MAX_VALUE;
     
     return nearest(root, p, nearest, min, 0);
   }
   
   private Point2D nearest(Node node, Point2D p, Point2D nearest, double distance, int level) {
     if (node == null) return nearest;
     double distanceCh = node.point.distanceSquaredTo(p);
     if (distanceCh < distance) {
       nearest = node.point;
       distance = distanceCh;
     }
     Node fst, scd;
     int cmp = compare(node, p, level);
     if (cmp < 0) {
       fst = node.lt;
       scd = node.gt;
     }
     else {
       fst = node.gt;
       scd = node.lt;
     }
     level++;
     if (fst != null && fst.rect.distanceSquaredTo(p) < distance) {
       nearest = nearest(fst, p, nearest, distance, level);
       distance = nearest.distanceSquaredTo(p);
     }
     if (scd != null && scd.rect.distanceSquaredTo(p) < distance) {
       nearest = nearest(scd, p, nearest, distance, level);
     }
     return nearest;
   }

   public static void main(String[] args) {                 // unit testing of the methods (optional)
     KdTree kdtree = new KdTree();
     Point2D point = new Point2D(0.206107, 0.904508);
     StdDraw.setPenRadius(0.01);
     StdDraw.setPenColor(StdDraw.BLACK);
     point.draw();
     kdtree.insert(new Point2D(0.206107, 0.095492));
     kdtree.insert(new Point2D(0.975528, 0.654508));
     kdtree.insert(new Point2D(0.024472, 0.345492));
     kdtree.insert(new Point2D(0.793893, 0.095492));
     kdtree.insert(new Point2D(0.793893, 0.904508));
     kdtree.insert(new Point2D(0.975528, 0.345492));
     kdtree.insert(new Point2D(0.206107, 0.904508));
     kdtree.insert(new Point2D(0.500000, 0.000000));
     kdtree.insert(new Point2D(0.024472, 0.654508));
     kdtree.insert(new Point2D(0.500000, 1.000000));
     StdOut.println(kdtree.n);
     
     StdOut.println(kdtree.contains(point));
     kdtree.draw();
     
     StdDraw.show();
     RectHV rect = new RectHV(0.0, 0.0, 1.0, 1.0);
     StdDraw.enableDoubleBuffering();
     kdtree = new KdTree();
     while (true) {
         if (StdDraw.mousePressed()) {
             double x = StdDraw.mouseX();
             double y = StdDraw.mouseY();
             StdOut.printf("%8.6f %8.6f\n", x, y);
             Point2D p = new Point2D(x, y);
             if (rect.contains(p)) {
                 StdOut.printf("%8.6f %8.6f\n", x, y);
                 kdtree.insert(p);
                 StdDraw.clear();
                 kdtree.draw();
                 StdDraw.show();
             }
         }
         StdDraw.pause(50);
     }
     
   }
}