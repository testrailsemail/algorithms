import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class Outcast {
  private final WordNet net;
  
  public Outcast(WordNet wordnet) {         // constructor takes a WordNet object
    net = wordnet;
  }
   
  public String outcast(String[] nouns) {  // given an array of WordNet nouns, return an outcast
    int maxDistance = -1;
    String outcast = null;
    
    for (String noun1 : nouns) {
      int distance = 0;
      for (String noun2 : nouns)
        if (!noun1.equals(noun2)) {
          distance += net.distance(noun1, noun2);
        }
      if (distance > maxDistance) {
        maxDistance = distance;
        outcast = noun1;
      }
    }
    
    return outcast;
  }
   
  public static void main(String[] args) { // see test client below
    WordNet wordnet = new WordNet(args[0], args[1]);
    Outcast outcast = new Outcast(wordnet);
    for (int t = 2; t < args.length; t++) {
      In in = new In(args[t]);
      String[] nouns = in.readAllStrings();
      StdOut.println(args[t] + ": " + outcast.outcast(nouns));
    }     
  }
}
