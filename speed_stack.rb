class Node
  attr_accessor :next_node, :item
  def initialize(item = nil, next_node = nil)
    @item = item
    @next_node = next_node
  end
end


class Stack
  def initialize
    @first = nil
  end

  def push(item)
    @first = Node.new(item, @first)
  end
  alias_method :"<<", :push

  def pop
    raise "Stack is empty" if empty?
    item = @first.item
    @first = @first.next_node
    item
  end

  def empty?
    @first.nil?
  end
end

require 'benchmark'
require "memory_profiler"
# require_relative 'queue'
# require_relative 'queueFast'

GC.disable


number_of_elements = 3_600_000

a1 =[]
a2 = []
a3 = []
a4 = []

s1 = Stack.new
s2 = Stack.new
# q4 = Que.new

# q6 = QueFast.new


puts number_of_elements


# MEMORY**************************************************************

# report = MemoryProfiler.report do
  # q1 = Queue.new
  # number_of_elements.times do
    # a1.unshift(true)
  # end
# end
# report.pretty_print

# a1 = []

# report = MemoryProfiler.report do

#   number_of_elements.times do
#     s1 << true
#   end
# end
# report.pretty_print

# s1 = Stack.new


number_of_elements.times do
  # s1.push(true)
  s2.push(true)
  a1 << true
  # a2 << true
  a3 << true
  # a4 << true
end

number_of_operations = 1000000#000

Benchmark.bm do |bm|
  puts "Satck#push('test')"
  bm.report do
    number_of_operations.times { s1.push('test') }
  end

  puts "Stack#pop"
  bm.report do
    number_of_operations.times { s2.pop }
  end

  puts "Array#shift"
  bm.report do
    number_of_operations.times { a1.shift }
  end

  puts "Array#unshift"
  bm.report do
    number_of_operations.times { a2.unshift('test') }
  end

  puts "Array#pop"
  bm.report do
    number_of_operations.times { a3.pop }
  end

  puts "Array#<<"
  bm.report do
    number_of_operations.times { a4 << 'test' }
  end
end
