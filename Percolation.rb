# **
#  *  The {@code WeightedQuickUnionUF} class represents a <em>union–find data type</em>
#  *  (also known as the <em>disjoint-sets data type</em>).
#  *  It supports the <em>union</em> and <em>find</em> operations,
#  *  along with a <em>connected</em> operation for determining whether
#  *  two sites are in the same component and a <em>count</em> operation that
#  *  returns the total number of components.
#  *  <p>
#  *  The union–find data type models connectivity among a set of <em>n</em>
#  *  sites, named 0 through <em>n</em>–1.
#  *  The <em>is-connected-to</em> relation must be an 
#  *  <em>equivalence relation</em>:
#  *  <ul>
#  *  <li> <em>Reflexive</em>: <em>p</em> is connected to <em>p</em>.
#  *  <li> <em>Symmetric</em>: If <em>p</em> is connected to <em>q</em>,
#  *       then <em>q</em> is connected to <em>p</em>.
#  *  <li> <em>Transitive</em>: If <em>p</em> is connected to <em>q</em>
#  *       and <em>q</em> is connected to <em>r</em>, then
#  *       <em>p</em> is connected to <em>r</em>.
#  *  </ul>
#  *  <p>
#  *  An equivalence relation partitions the sites into
#  *  <em>equivalence classes</em> (or <em>components</em>). In this case,
#  *  two sites are in the same component if and only if they are connected.
#  *  Both sites and components are identified with integers between 0 and
#  *  <em>n</em>–1. 
#  *  Initially, there are <em>n</em> components, with each site in its
#  *  own component.  The <em>component identifier</em> of a component
#  *  (also known as the <em>root</em>, <em>canonical element</em>, <em>leader</em>,
#  *  or <em>set representative</em>) is one of the sites in the component:
#  *  two sites have the same component identifier if and only if they are
#  *  in the same component.
#  *  <ul>
#  *  <li><em>union</em>(<em>p</em>, <em>q</em>) adds a
#  *      connection between the two sites <em>p</em> and <em>q</em>.
#  *      If <em>p</em> and <em>q</em> are in different components,
#  *      then it replaces
#  *      these two components with a new component that is the union of
#  *      the two.
#  *  <li><em>find</em>(<em>p</em>) returns the component
#  *      identifier of the component containing <em>p</em>.
#  *  <li><em>connected</em>(<em>p</em>, <em>q</em>)
#  *      returns true if both <em>p</em> and <em>q</em>
#  *      are in the same component, and false otherwise.
#  *  <li><em>count</em>() returns the number of components.
#  *  </ul>
#  *  <p>
#  *  The component identifier of a component can change
#  *  only when the component itself changes during a call to
#  *  <em>union</em>—it cannot change during a call
#  *  to <em>find</em>, <em>connected</em>, or <em>count</em>.
#  *  <p>
#  *  This implementation uses weighted quick union by size (without path compression).
#  *  Initializing a data structure with <em>n</em> sites takes linear time.
#  *  Afterwards, the <em>union</em>, <em>find</em>, and <em>connected</em>
#  *  operations  take logarithmic time (in the worst case) and the
#  *  <em>count</em> operation takes constant time.
#  *  For alternate implementations of the same API, see
#  *  {@link UF}, {@link QuickFindUF}, and {@link QuickUnionUF}.
#  *
#  *  <p>
#  *  For additional documentation, see <a href="http://algs4.cs.princeton.edu/15uf">Section 1.5</a> of
#  *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
#  *
#  *  @author Robert Sedgewick
#  *  @author Kevin Wayne
#  *

class WeightedQuickUnionUF
  attr_reader :count#, :size, :parent
  
  def initialize(n)
    
    raise if !n.kind_of? Integer
    
    @count = n    # number of components
    @parent = []  # parent[i] = parent of i
    @size = []    # size[i] = number of sites in subtree rooted at i 
    for i in 0..n-1 do
      @parent[i] = i
      @size[i] = 1
    end
  end

  def find(p)
    validate(p)
    while p != @parent[p] do
      p = @parent[p]
    end
    return p
  end

  def connected(p, q)
    return find(p) == find(q)
  end

  def union(p, q)
    rootP = find(p)
    rootQ = find(q)
    return if rootP == rootQ

    if @size[rootP] < @size[rootQ]
        @parent[rootP] = rootQ
        @size[rootQ] += @size[rootP]
    else
        @parent[rootQ] = rootP
        @size[rootP] += @size[rootQ]
    end
    
    @count -= 1
  end

  def self.test(n)
    uf = WeightedQuickUnionUF.new(n)
    input = "1"
    while (input != "\n") do
      puts "two integers between 0 and #{n-1} or any key to abort"
      input = gets
      p, q = input.chomp.split
      p = p.to_i
      q = q.to_i
      begin
        next if uf.connected(p, q)
        uf.union(p, q)
      rescue  
        puts "must be two integers between 0 and #{n-1}"
      end
      puts "#{p} #{q}"
    end
    puts "#{uf.count} components"
  end


  private

  def validate(p)
    n = @parent.length
    raise IndexError if (p < 0 || p >= n)
  end

end

class Percolation
  
  def initialize (n) # create n-by-n grid, with all sites blocked
  
    if (n <= 0) 
      raise "n must be a number bigger than 0"
    end
    @site = Array.new(n+1) { Array.new(n+1, 0) } # make an 2d array and populate in with '0', prog needs 1..n indexes
    @n = n
    @size = n * n + 2
    @filled = @size - 2
    @toFill = @size - 1
        
    @id = WeightedQuickUnionUF.new(@size)
    @opened = 0
  end
 
  def open(row, col)    # open site (row, col) if it is not open already
    validate(row, col)
    if !isOpen(row, col)
      p = trans(row, col)
  # System.out.println("opened id: " + p);
      @id.union(p, @toFill) if (row == @n) 

      if (row == 1) 
        @id.union(@filled, p)
      else 
        neighbors(row, col).each do |neighbor|
          n = trans(neighbor)
          if isFull(neighbor)
            @id.union(p, n)
            break
          end
          
          @id.union(p, n) if isOpen(neighbor)
        end      
      end
      if isFull(row, col)
        neighbors(row, col).each do |neighbor|
          n = trans(neighbor)
          @id.union(n, p) if isOpen(neighbor) && !isFull(neighbor)
        end
      end
      @opened += 1
      @site[row][col] = 1
    end
        
  end

  def isOpen(*args) # is site (row, col) open?
    if args.length == 1
      row, col = args[0]
    else
      row, col = args
    end
      validate(row, col)
      return @site[row][col] == 1
  end
  
  def isFull(*args)  # is site (row, col) full?
    if args.length == 1
      row, col = args[0]
    else
      row, col = args
    end
    validate(row, col)
    p = trans(row, col)
    return @id.connected(p, @filled)
  end

  def numberOfOpenSites       # number of open sites
    return @opened
  end

  def percolates              # does the system percolate?
# //        for (int j = 1; j <= high; j++) {
# //            if (isFull(high, j)) return true; 
# //        }
    return @id.connected(@toFill, @filled)
  end

  def self.test(n)
    prng = Random.new
    perc = Percolation.new(n)
    key = "1"
    while !perc.percolates && key != "\n" do
      row = prng.rand(1..n)
      col = prng.rand(1..n)
      puts "row: #{row}, col: #{col}"
      begin
        open(row, col)
      rescue  
        puts "must be two integers between 1 and #{@n}"
      end     
      puts "Opened sites:   #{numberOfOpenSites}"
      puts "percolates:     #{percolates}"
      key = gets 
    end

    puts perc.site
  end

  private

  def validate(row, col)
    forRow = row <= 0 || row > @n
    forCol = col <= 0 || col > @n
    raise IndexError if forRow || forCol
  end

  def trans(*args)
    if args.length == 1
      row, col = args[0]
    else
      row, col = args
    end
    return @n * (row - 1) + (col - 1) % @n
  end
  
  def transB(n)    
    i = n / @n + 1
    j = n % @n + 1
    return i,j
  end

  def neighbors(row, col)
    neighbor = []
    
    for y in [1, col - 1].max..[col + 1, @n].min do
      # neighbor.push(trans(row, y)) if (y != col)
      neighbor.push([row, y]) if (y != col)
    end
    for x in [1, row-1].max..[row+1, @n].min do
      # neighbor.push(trans(x, col))  if (x != row)
      neighbor.push([x, col])  if (x != row)
    end
    
    return neighbor
  end
end


class PercolationStats
  
  def initialize(n, numtrials)    # perform trials independent experiments on an n-by-n grid
    #Exception if either n <= 0 or trials <= 0
   
    @trials = numtrials;
    
    raise "num and trials must be a number bigger than 0" if (n <= 0 || @trials <= 0)
    
    @experiments = [];
    prng = Random.new

    @trials.times do |i|  
      perc = Percolation.new(n)
      while !perc.percolates do
        row = prng.rand(1..n)
        col = prng.rand(1..n)
       
        perc.open(row, col)
      end 
      @experiments[i] = 1.0*perc.numberOfOpenSites/(n*n)
    end
  end
  
  def mean                          # sample mean of percolation threshold
    sum = @experiments.count > 0 ? @experiments.reduce(0, :+) : 0.0
    avg = sum / @experiments.count.to_f 
  end
   
  def stddev                        # sample standard deviation of percolation threshold
    m = mean
    stdev = @experiments.count > 0 ? ( @experiments.reduce(0){ |accum, v| accum + (m - v) ** 2 } / (@experiments.count - 1) ) ** 0.5 : 0.0
  end

  def confidenceLo                  # low  endpoint of 95% confidence interval
    mean - (1.96*stddev)/(1.0*@trials)**0.5
  end

  def confidenceHi                  # high endpoint of 95% confidence interval
    mean + (1.96*stddev)/(1.0*@trials)**0.5
  end

  def self.test(*args)      # test client (described below)
    n, trials = args
    #n = 20;
    #int trials = 100;
    start = PercolationStats.new(n, trials)
    puts "mean                    = #{start.mean};"
    puts "stddev                  = #{start.stddev};"
    puts "95% confidence interval = [#{start.confidenceLo}, #{start.confidenceHi}]"
     
  end
end

require 'benchmark'

time = Benchmark.measure { PercolationStats.new(20, 100)
 }
puts time.real