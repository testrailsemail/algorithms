import java.util.Arrays;

import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdRandom;

public class Board {
  private final int n;
//  private int moves;
  private final int[][] tiles;
//  private int[][] goal;
  
    public Board(int[][] blocks) {                          // construct a board from an n-by-n array of blocks
      if (blocks == null) throw new NullPointerException(); // (where blocks[i][j] = block in row i, column j)
      
      n = blocks.length;
      this.tiles = new int[n][n];
      
      for (int i = 0; i < n; i++) {
        System.arraycopy(blocks[i], 0, this.tiles[i], 0, n);
        if (i <= n-2 && blocks[i].length != blocks[i+1].length)
          throw new IllegalArgumentException("wrong board size detected");
      }
    }
                                           
    public int dimension() {                 // board dimension n
      return n;
    }
    
    public int hamming() {                   // number of blocks out of place
      int count = 0;
      for (int i = 0; i < n; i++) 
        for (int j = 0; j < n; j++)
          if (tiles[i][j] != 0)
            if ((tiles[i][j] != 1 + n*i+j)) count++;
      return count;  
    }
    
    public int manhattan() {                // sum of Manhattan distances between blocks and goal
      int count = 0;
      for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
          if ((tiles[i][j] != 0)) 
            count += Math.abs((tiles[i][j] - 1)/n - i) + Math.abs((tiles[i][j] - 1) % n - j);
        }
      }
      return count;      
    }
    
    public boolean isGoal() {               // is this board the goal board?
      int[][] goal = new int[n][n];
      for (int i = 0; i < n; i++) 
        for (int j = 0; j < n; j++)
          goal[i][j] = 1 + n*i+j;
      goal[n-1][n-1] = 0;
      return Arrays.deepEquals(this.tiles, goal);
    }
    
    public Board twin() {                   // a board that is obtained by exchanging any pair of blocks
      int[][] arr = new int[n][n];
      int[] pointers = new int[4];
      int flag = 0;
      for (int i = 0; i < n; i++) 
        System.arraycopy(this.tiles[i], 0, arr[i], 0, n);
      while (flag != 1) {
        for (int i = 0; i < 4; i++)
          pointers[i] = StdRandom.uniform(n);
        boolean p = pointers[0] == pointers[2] && pointers[1] == pointers[3];
        if (!p && arr[pointers[0]][pointers[1]] != 0 && arr[pointers[2]][pointers[3]] != 0) {
          int temp = arr[pointers[0]][pointers[1]]; 
          arr[pointers[0]][pointers[1]] = arr[pointers[2]][pointers[3]];
          arr[pointers[2]][pointers[3]] = temp;
          flag = 1;
        }
      }
      Board twin = new Board(arr);   
      return twin;
    }
    
    private int[][] exch(int[][] a, int i, int j, int k, int l) {
//      int[][] b = a.clone();
      int[][] b = new int[n][n];
      for (int m = 0; m < n; m++)
        System.arraycopy(a[m], 0, b[m], 0, n);
      int temp = b[i][j];
      b[i][j] = b[k][l];
      b[k][l] = temp;
      return b;
    }
    
    public boolean equals(Object other) {       // does this board equal y?
      if (other == this) return true;
      if (other == null) return false;
      if (other.getClass() != this.getClass()) return false;
      Board that = (Board) other;
      return (Arrays.deepEquals(this.tiles, that.tiles));
//      return (this.toString().equals(that.toString())); 
    }
    
    public Iterable<Board> neighbors() {    // all neighboring boards
//      return new Stack<Board>() {
        Stack<Board> neighbors = new Stack<Board>();
        int row = -1;
        int col = -1;
        outerloop:
        for (int i = 0; i < n; i++) 
          for (int j = 0; j < n; j++) 
            if (tiles[i][j] == 0) {
              row = i;
              col = j;
              break outerloop;
            }
        for (int y = Math.max(0, col - 1); y <= Math.min(col + 1, n - 1); y++) {
          if (y != col) {
            int[][] arr = exch(tiles, row, col, row, y);
            Board temp = new Board(arr);
            neighbors.push(temp);
          }
        }
        for (int x = Math.max(0, row - 1); x <= Math.min(row + 1, n - 1); x++) {
            if (x != row) {
              int[][] arr = exch(tiles, row, col, x, col);
              Board temp = new Board(arr);
              neighbors.push(temp);
            }
        }
        return neighbors;
    }
    
    public String toString() {              // string representation of this board (in the output format specified below)
      StringBuilder s = new StringBuilder();
      s.append(n + "\n");
      for (int i = 0; i < n; i++) {
          for (int j = 0; j < n; j++) {
              s.append(String.format("%2d ", tiles[i][j]));
          }
          s.append("\n");
      }
      return s.toString();
    }

    public static void main(String[] args) { // unit tests (not graded)
      int[][] arr = new int[3][3];
      int inc = 1;
      int n = 3;
      for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
          arr[i][j] = inc;
          inc++;
        }
      }
      arr[n-2][n-2] = 0;
      Board bo = new Board(arr);
      System.out.println("equality check: " + bo.isGoal());
      System.out.println("initial board:  " + bo);
      for (Board neighbor : bo.neighbors())
        System.out.println("neighbor board:  " + neighbor);
      System.out.println("twin board:  " + bo.twin());
      System.out.println("initial board:  " + bo);
      
    }
}