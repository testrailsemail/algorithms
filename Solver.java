
import java.util.Comparator;
import java.util.NoSuchElementException;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;

public class Solver {
  private int moves;
//  private Node initialNode;
//  private Node prevNode;
  
  private boolean isSolvable;
//  private boolean twinSolvable;
  private Node goalNode;
  
  // helper linked list class
   private class Node { 
       private Board board;
       private int moves;
       private Node previous;
       
       private Node(Board item, int moves, Node previousNode) {
         this.board = item;
         this.moves = moves;
         this.previous = previousNode;
       }
       
//       private Node(Board item, int moves) {
//         this.board = item;
//         this.moves = moves;
//         this.previous = null;
//       }
       
       private int priorityM() {
         int pri = this.moves + this.board.manhattan();
         return pri;
       }
       
       private int priorityH() {
         int pri = this.moves + this.board.hamming();
         return pri;
       }
       
//       public int compareTo(Node that) {
//         if (that == null)
//           throw new NullPointerException("one of the nodes is null");
//         if (this.priority() < that.priority()) return -1;
//         else if (this.priority() == that.priority())
//           return 0;
//           else return 1;
//       }
   }
   
    public Solver(Board initial) {           // find a solution to the initial board (using the A* algorithm)
      if (initial == null) 
        throw new NullPointerException("null initial puzzle board");
      
      moves = 0;
      goalNode = null;
      Node initialNode = new Node(initial, moves, null);
      Node initialTwin = new Node(initial.twin(), moves, null);
//      prevNode = null;
//      Node prevTwin = null;
      
      MinPQ<Node> pq = new MinPQ<Node>(this.manhattan());
      MinPQ<Node> pqTwin = new MinPQ<Node>(this.manhattan());
      
      pq.insert(initialNode);
      pqTwin.insert(initialTwin);
      
      Node current = initialNode; // pq.delMin();
      Node currentTwin = initialTwin; // pqTwin.delMin();
      
      boolean initialSolved = false;
      boolean twinSolved = false;
      int steps = 0;
//      Board b;
//      System.out.println("Current twin: " + currentTwin.board);
      int movesTwin = 0;
      while (!initialSolved && !twinSolved) {
        current = pq.delMin();
        moves = current.moves;
//        try { current = pq.delMin(); } 
//        catch (NoSuchElementException e) {
//          this.isSolvable = false;
//          this.moves = -1;
//          return;
//        }
//        System.out.println("Current: " + current.board);
//        System.out.println("Current b: " + current.board.manhattan());
//        System.out.println("Current n: " + current.priorityM());
        try { currentTwin = pqTwin.delMin(); }
        catch (NoSuchElementException e) {
//          System.out.println("Exception");
          twinSolved = true;
        }
        movesTwin = currentTwin.moves;
        if (current.board.isGoal()) {
          this.isSolvable = true;
          initialSolved = true;
          this.goalNode = current;
          break;
        }
        if (currentTwin.board.isGoal() || twinSolved) {
//          twinSolvable = true;
//          System.out.println("twin board:  " + twinSolvable);
          twinSolved = true;
          this.moves = -1;
          break;  
//          if (++steps > Math.pow(initial.dimension(), initial.dimension())) {
//            this.isSolvable = false;
////            System.out.println("NOT NOT");
//            break;
//          }
        }
//        if (current.previous == null)
//          b = null;
//        else b = current.previous.board;
        
        moves++;
        movesTwin++;
//        System.out.println("Move:   " + moves);
        
        boolean flag;
        for (Board n : current.board.neighbors()) {
          Node temp = current;
          flag = true;
          while (temp.previous != null) {
            if (n.equals(temp.previous.board)) {
              flag = false;
              break;
            }
            temp = temp.previous;
          }
          if (flag)
            pq.insert(new Node(n, moves, current));
//          if (!n.equals(b)) {
////            System.out.println("neighbor board:  " + n);
//            pq.insert(new Node(n, moves, current));
////            System.out.println("Inserted");
//          }
        }
        
        if (!twinSolved) {
//          if (currentTwin.previous != null)
//            b = currentTwin.previous.board;
//          else b = null;
          for (Board n : currentTwin.board.neighbors()) {
            flag = true;
            Node temp = currentTwin;
            while (temp.previous != null) {
              if (n.equals(temp.previous.board)) {
                flag = false;
                break;
              }
              temp = temp.previous;
            }
            if (flag)
              pqTwin.insert(new Node(n, movesTwin, current));
//            System.out.println("neighbor board:  " + n);
//            if (!n.equals(b))
//              pqTwin.insert(new Node(n, moves, currentTwin));
          }
        }
//        System.out.println(initialSolved);
//        System.out.println(twinSolved);
      }
//      System.out.println("while ended");
    }
    
//    private static class ByManhattan implements Comparator<Node> {
    private Comparator<Node> manhattan() {
      return new Comparator<Node>() {
        @Override
        public int compare(Node o1, Node o2) {
            return Integer.compare(o1.priorityM(), o2.priorityM());
        }
      };
    }
    
    private Comparator<Node> hamming() {
      return new Comparator<Node>() {
        @Override
        public int compare(Node o1, Node o2) {
            return Integer.compare(o1.priorityH(), o2.priorityH());
        }
      };
    }
    
    public boolean isSolvable() {           // is the initial board solvable?
      return isSolvable;
    }
    
    public int moves() {                    // min number of moves to solve initial board; -1 if unsolvable
      return moves;
    }
    
    public Iterable<Board> solution() {     // sequence of boards in a shortest solution; null if unsolvable
      if (!isSolvable()) return null;
      Stack<Board> solution = new Stack<Board>();
      Node current = goalNode;
      while (current != null) {
        solution.push(current.board);
        current = current.previous;
      }
      return solution;
    }
    
    public static void main(String[] args) { // solve a slider puzzle (given below)
      // create initial board from file
      In in = new In(args[0]);
      int n = in.readInt();
      int[][] blocks = new int[n][n];
      for (int i = 0; i < n; i++)
          for (int j = 0; j < n; j++)
              blocks[i][j] = in.readInt();
      Board initial = new Board(blocks);

      // solve the puzzle
      Solver solver = new Solver(initial);

      // print solution to standard output
      if (!solver.isSolvable())
          StdOut.println("No solution possible");
      else {
          StdOut.println("Minimum number of moves = " + solver.moves());
          for (Board board : solver.solution())
              StdOut.println(board);
      }
    }
}