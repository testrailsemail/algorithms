// import java.util.Arrays;
import java.util.Comparator;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

public class Point implements Comparable<Point> {

    private final int x;     // x-coordinate of this point
    private final int y;     // y-coordinate of this point

    /**
     * Initializes a new point.
     *
     * @param  x the <em>x</em>-coordinate of the point
     * @param  y the <em>y</em>-coordinate of the point
     */
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Draws this point to standard draw.
     */
    public void draw() {
        StdDraw.point(x, y);
    }

    /**
     * Draws the line segment between this point and the specified point
     * to standard draw.
     *
     * @param that the other point
     */
    public void drawTo(Point that) {
        StdDraw.line(this.x, this.y, that.x, that.y);
    }

    /**
     * Returns the slope between this point and the specified point.
     * Formally, if the two points are (x0, y0) and (x1, y1), then the slope
     * is (y1 - y0) / (x1 - x0). For completeness, the slope is defined to be
     * +0.0 if the line segment connecting the two points is horizontal;
     * Double.POSITIVE_INFINITY if the line segment is vertical;
     * and Double.NEGATIVE_INFINITY if (x0, y0) and (x1, y1) are equal.
     *
     * @param  that the other point
     * @return the slope between this point and the specified point
     */
    public double slopeTo(Point that) {
      double tg;
      if (that.x == x && that.y == y) return Double.NEGATIVE_INFINITY;
      else if (that.x == x) return Double.POSITIVE_INFINITY;
      else if (that.y == y) return +0.0;
      else tg = (double) (that.y - y)/(that.x - x);
      return tg;
    }

    /**
     * Compares two points by y-coordinate, breaking ties by x-coordinate.
     * Formally, the invoking point (x0, y0) is less than the argument point
     * (x1, y1) if and only if either y0 < y1 or if y0 = y1 and x0 < x1.
     *
     * @param  that the other point
     * @return the value <tt>0</tt> if this point is equal to the argument
     *         point (x0 = x1 and y0 = y1);
     *         a negative integer if this point is less than the argument
     *         point; and a positive integer if this point is greater than the
     *         argument point
     */
    public int compareTo(Point that) {
      if (that == null)
        throw new NullPointerException("one of the points is null");
        if (y < that.y) return -1;
        else if (y == that.y) {
          if (x < that.x) return -1;
          else if (x == that.x) return 0;
          else return 1;
        }
        return 1;
    }

    /**
     * Compares two points by the slope they make with this point.
     * The slope is defined as in the slopeTo() method.
     *
     * @return the Comparator that defines this ordering on points
     */
    public Comparator<Point> slopeOrder() {
//      return (o1, o2) -> Double.compare(slopeTo(o1), slopeTo(o2));  
      return new Comparator<Point>() {
        @Override
        public int compare(Point o1, Point o2) {
            return Double.compare(slopeTo(o1), slopeTo(o2));
        }
      };
    }

    /**
     * Returns a string representation of this point.
     * This method is provide for debugging;
     * your program should not rely on the format of the string representation.
     *
     * @return a string representation of this point
     */
    public String toString() {
        /* DO NOT MODIFY */
        return "(" + x + ", " + y + ")";
    }

    /**
     * Unit tests the Point data type.
     */
    public static void main(String[] args) {
//        Point x = new Point(1, 2);
//        Point x1 = new Point(2, 1);
//        Point y = new Point(1, 3);
//        Point z = new Point(-1, -2);
//        Point a = new Point(-1, 2);
//        Point b = new Point(4, 2);
//        Point c = new Point(1, 4);
//        Point[] poi = new Point[7];
//        for (int i = 0; i < 7; i++) {
//          poi[i] = new Point(i, i);
//        }
//        Point[] points = {a, y, c, x1, z, b, x, poi[0], poi[1], poi[2], poi[3], poi[4]};
//        StdOut.println("x:    " + x + " y: " + y + " a: " + a);
//        StdOut.println("x, y:  " + x.compareTo(y)+ "; x, a:   " + x.compareTo(a));
//        StdOut.println("Points unsorted: ");
//        for (Point each : points) StdOut.print(" " + each);
//        StdOut.println();
//        Arrays.sort(points);
//        StdOut.println("Points sorted: ");
//        for (Point each : points) StdOut.print(" " + each);
//        StdOut.println();
//        Arrays.sort(points, x1.slopeOrder());
//        StdOut.println("Points sorted by slope to x1:");
//        for (Point each : points) StdOut.print(" " + each);
//        StdOut.println();
////        x.draw();
////        LineSegment line = new LineSegment(x, a);
////        line.draw();
//        Point[] poin = {a, y, c, x1, z, b, x, poi[0], poi[1], poi[2], poi[3], poi[4], poi[5], poi[6]};
////        BruteCollinearPoints p = new BruteCollinearPoints(poin);
////        StdOut.println();
////        StdOut.println("Segments:");
////        System.out.println(p.numberOfSegments());
////        for (LineSegment segment : p.segments()) {
////          StdOut.println(" " + segment + "; ");
////        }
//        FastCollinearPoints t = new FastCollinearPoints(poin);
//        StdOut.println(t.numberOfSegments());
   // read the n points from a file
      In in = new In(args[0]);
      int n = in.readInt();
      Point[] points = new Point[n];
      for (int i = 0; i < n; i++) {
          int x = in.readInt();
          int y = in.readInt();
          points[i] = new Point(x, y);
      }

      // draw the points
      StdDraw.enableDoubleBuffering();
      StdDraw.setXscale(0, 32768);
      StdDraw.setYscale(0, 32768);
      for (Point p : points) {
          p.draw();
      }
      StdDraw.show();

      // print and draw the line segments
      FastCollinearPoints collinear = new FastCollinearPoints(points);
      for (LineSegment segment : collinear.segments()) {
          StdOut.println(segment);
          segment.draw();
      }
      StdDraw.show();
    }
}