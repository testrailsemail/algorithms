# The program is a conversion of Princeton Digraph class to ruby
# Author:: ez (testrailsemail@gmail.com)

#******************************************************************************
#    Compilation:  javac Digraph.java
#    Execution:    java Digraph filename.txt
#    Dependencies: Bag.java In.java StdOut.java
#    Data files:   https://algs4.cs.princeton.edu/42digraph/tinyDG.txt
#                  https://algs4.cs.princeton.edu/42digraph/mediumDG.txt
#                  https://algs4.cs.princeton.edu/42digraph/largeDG.txt
#
#    A graph, implemented using an array of lists.
#    Parallel edges and self-loops are permitted.
#
#    % java Digraph tinyDG.txt
#    13 vertices, 22 edges
#    0: 5 1
#    1:
#    2: 0 3
#    3: 5 2
#    4: 3 2
#    5: 4
#    6: 9 4 8 0
#    7: 6 9
#    8: 6
#    9: 11 10
#    10: 12
#    11: 4 12
#    12: 9
#
#  ******************************************************************************

# The {@code Digraph} class represents a directed graph of vertices
# named 0 through <em>V</em> - 1.
# It supports the following two primary operations: add an edge to the digraph,
# iterate over all of the vertices adjacent from a given vertex.
# Parallel edges and self-loops are permitted.
# <p>
# This implementation uses an adjacency-lists representation, which
# is a vertex-indexed array of {@link Bag} objects.
# All operations take constant time (in the worst case) except
# iterating over the vertices adjacent from a given vertex, which takes
# time proportional to the number of such vertices.
# <p>
# For additional documentation,
# see <a href="https://algs4.cs.princeton.edu/42digraph">Section 4.2</a> of
# <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.

# @author Robert Sedgewick
# @author Kevin Wayne

# require 'io'
class Digraph
  attr_reader :v
  attr_accessor :e, :indegree, :adj
    # private static final String NEWLINE = System.getProperty("line.separator");

  # Initializes an empty digraph with <em>v</em> vertices.
  # @param v the number of vertices
  # @throws ArgumentError if {@code V < 0}
  def initialize(vertices)
    raise ArgumentError.new("Number of vertices in a Digraph must be nonnegative") if vertices < 0
    @v = vertices                   # number of vertices in this digraph
    @e = 0                          # number of edges in this digraph
    @indegree = Array.new(@v, 0)    # indegree of vertex v
    @adj = {}                       # adjacency list for vertex v
  end

  # Initializes a digraph from the specified input stream.
  # The format is the number of vertices <em>V</em>,
  # followed by the number of edges <em>E</em>,
  # followed by <em>E</em> pairs of vertices, with each entry separated by whitespace.
  # @param  in the input stream
  # @throws IllegalArgumentException if the endpoints of any edge are not in prescribed range
  # @throws IllegalArgumentException if the number of vertices or edges is negative
  # @throws IllegalArgumentException if the input stream is in the wrong format
  def self.from_io(inp)
    begin
      vertices = inp.readline.to_i
      dg = self.new(vertices)

      e = inp.readline.to_i
      raise ArgumentError.new("Number of edges in a Digraph must be nonnegative") if e < 0
      (0...e).each do
        line = inp.readline.split(" ")
        v = line[0].to_i
        w = line[1].to_i
        dg.addEdge(v, w)
      end
      dg
    rescue EOFError, NoMethodError
      raise ArgumentError.new("invalid input format in Digraph constructor");
    end
  end


  # Initializes a new digraph that is a deep copy of the specified digraph.
  # @param  G the digraph to copy
  def self.from_graph(graph)
    dg = self.new(graph.v)
    dg.e = graph.e
    dg.indegree = graph.indegree.clone
    dg.adj = graph.adj.clone
    dg
  end

  # Adds the directed edge v→w to this digraph.
  # @param  v the tail vertex
  # @param  w the head vertex
  # @throws IllegalArgumentException unless both {@code 0 <= v < V} and {@code 0 <= w < V}
  def addEdge(v, w)
    validateVertex(v)
    validateVertex(w)
    (@adj[v] ||= []) << w
    @indegree[w] += 1
    @e += 1
  end

  # Returns the vertices adjacent from vertex {@code v} in this digraph.
  # @param  v the vertex
  # @return the vertices adjacent from vertex {@code v} in this digraph, as an iterable
  # @throws IllegalArgumentException unless {@code 0 <= v < V}
  def adjacent(vertex)
    validateVertex(vertex)
    (@adj[vertex] ||= []).clone
  end

  # Returns the number of directed edges incident from vertex {@code v}.
  # This is known as the <em>outdegree</em> of vertex {@code v}.
  # @param  v the vertex
  # @return the outdegree of vertex {@code v}
  # @throws IllegalArgumentException unless {@code 0 <= v < V}
  def outdegree(vertex)
    self.adj[vertex].size
  end

  # Returns the number of directed edges incident to vertex {@code v}.
  # This is known as the <em>indegree</em> of vertex {@code v}.
  # @param  v the vertex
  # @return the indegree of vertex {@code v}
  # @throws IllegalArgumentException unless {@code 0 <= v < V}
  def indegree_Of(vertex)
    validateVertex(vertex)
    @indegree[vertex]
  end

  # Returns the reverse of the digraph.
  # @return the reverse of the digraph
  def reverse()
    reverse = Digraph.new(v)
    for x in 0...v
      self.adjacent(x).each { |w| reverse.addEdge(w, x) }
    end
    reverse
  end

  # Returns a string representation of the graph.
  # @return the number of vertices <em>V</em>, followed by the number of edges <em>E</em>,
  # followed by the <em>V</em> adjacency lists
  def to_s
    s = "#{v} vertices, #{e} edges" + "\n"
    for x in 0...v
      s += "#{x}: "
      self.adjacent(x).each {|w| s += "#{w} "}
      s += "\n"
    end
    s
  end

  # Unit tests the {@code Digraph} data type.
  # @param args the command-line arguments
  def self.main(args)
    inp = File.new(args)
    g =  Digraph.from_io(inp);
    puts g
    h = Digraph.from_graph(g);
    # print h
    r = h.reverse
    p h
    p r
  end

  # throw an IllegalArgumentException unless {@code 0 <= v < V}
  private
  def validateVertex(vertices)
    raise ArgumentError.new("vertex #{vertices} is not between 0 and #{v-1}") if vertices < 0 || vertices >= v
  end
end

# Digraph.main(ARGV[0])

