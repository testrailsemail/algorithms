# Transformed for ruby by testa19 (ez)
# /******************************************************************************
#  *  Compilation:  javac BreadthFirstDirectedPaths.java
#  *  Execution:    java BreadthFirstDirectedPaths digraph.txt s
#  *  Dependencies: Digraph.java Queue.java Stack.java
#  *  Data files:   https://algs4.cs.princeton.edu/42digraph/tinyDG.txt
#  *                https://algs4.cs.princeton.edu/42digraph/mediumDG.txt
#  *                https://algs4.cs.princeton.edu/42digraph/largeDG.txt
#  *
#  *  Run breadth-first search on a digraph.
#  *  Runs in O(E + V) time.
#  *
#  *  % java BreadthFirstDirectedPaths tinyDG.txt 3
#  *  3 to 0 (2):  3->2->0
#  *  3 to 1 (3):  3->2->0->1
#  *  3 to 2 (1):  3->2
#  *  3 to 3 (0):  3
#  *  3 to 4 (2):  3->5->4
#  *  3 to 5 (1):  3->5
#  *  3 to 6 (-):  not connected
#  *  3 to 7 (-):  not connected
#  *  3 to 8 (-):  not connected
#  *  3 to 9 (-):  not connected
#  *  3 to 10 (-):  not connected
#  *  3 to 11 (-):  not connected
#  *  3 to 12 (-):  not connected
#  *
#  ******************************************************************************/

# /**
#  *  The {@code BreadthDirectedFirstPaths} class represents a data type for finding
#  *  shortest paths (number of edges) from a source vertex <em>s</em>
#  *  (or set of source vertices) to every other vertex in the digraph.
#  *  <p>
#  *  This implementation uses breadth-first search.
#  *  The constructor takes time proportional to <em>V</em> + <em>E</em>,
#  *  where <em>V</em> is the number of vertices and <em>E</em> is the number of edges.
#  *  Each call to {@link #distTo(int)} and {@link #hasPathTo(int)} takes constant time;
#  *  each call to {@link #pathTo(int)} takes time proportional to the length
#  *  of the path.
#  *  It uses extra space (not including the digraph) proportional to <em>V</em>.
#  *  <p>
#  *  For additional documentation,
#  *  see <a href="https://algs4.cs.princeton.edu/42digraph">Section 4.2</a> of
#  *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
#  *
#  *  @author Robert Sedgewick
#  *  @author Kevin Wayne
#  */
require_relative 'digraph'
class BreadthFirstDirectedPaths
  INFINITY = Float::INFINITY
  attr_accessor :marked, :edgeTo, :distTo
  # marked[v] = is there an s->v path?
  # edgeTo[v] = last edge on shortest s->v path
  #distTo[v] = length of shortest s->v path

  # /**
  #  * Computes the shortest path from {@code s} and every other vertex in graph {@code G}.
  #  * @param G the digraph
  #  * @param s the source vertex
  #  * @throws IllegalArgumentException unless {@code 0 <= v < V}
  #  */
  def initialize(digraph, sourse)
    n = digraph.v
    @marked = Array.new(n, false)
    @distTo = Array.new(n, INFINITY)
    @edgeTo = []

    if sourse.respond_to? :each
      validateVertices(sourse)
      bfss(digraph, sourse)
    else
      validateVertex(sourse)
      bfs(digraph, sourse)
    end
  end

  # /**
  #  * Computes the shortest path from any one of the source vertices in {@code sources}
  #  * to every other vertex in graph {@code G}.
  #  * @param G the digraph
  #  * @param sources the source vertices
  #  * @throws IllegalArgumentException unless each vertex {@code v} in
  #  *         {@code sources} satisfies {@code 0 <= v < V}
  #  */

  # BFS from single source
  def bfs(digraph, source)
    q = Queue.new
    @marked[source] = true
    @distTo[source] = 0
    q << source
    until q.empty?
      v = q.deq
      digraph.adjacent(v).each do |w|
        if !@marked[w]
          @edgeTo[w] = v
          @distTo[w] = @distTo[v] + 1
          @marked[w] = true
          q.enq w
        end
      end
    end
  end

  # BFS from multiple sources
  def bfss(digraph, sources)
    q =  Queue.new
    sources.each do |s|
      @marked[s] = true
      @distTo[s] = 0
      q.enq s
    end
    until q.empty?
      v = q.deq
      digraph.adjacent(v).each do |w|
        if !@marked[w]
          @edgeTo[w] = v
          @distTo[w] = @distTo[v] + 1
          @marked[w] = true
          q.enq w
        end
      end
    end
  end

  # /**
  #  * Is there a directed path from the source {@code s} (or sources) to vertex {@code v}?
  #  * @param v the vertex
  #  * @return {@code true} if there is a directed path, {@code false} otherwise
  #  * @throws IllegalArgumentException unless {@code 0 <= v < V}
  #  */
  def hasPathTo?(v)
    validateVertex(v)
    @marked[v]
  end

  # /**
  #  * Returns the number of edges in a shortest path from the source {@code s}
  #  * (or sources) to vertex {@code v}?
  #  * @param v the vertex
  #  * @return the number of edges in a shortest path
  #  * @throws IllegalArgumentException unless {@code 0 <= v < V}
  #  */
  def distTo(v)
    validateVertex(v)
    @distTo[v]
  end

  # /**
  #  * Returns a shortest path from {@code s} (or sources) to {@code v}, or
  #  * {@code null} if no such path.
  #  * @param v the vertex
  #  * @return the sequence of vertices on a shortest path, as an Iterable
  #  * @throws IllegalArgumentException unless {@code 0 <= v < V}
  #  */
  def pathTo(v)
    validateVertex(v)

    return nil if !hasPathTo?(v)
    path = []
    x = v
    until @distTo[x] == 0
      path.unshift x
      x = @edgeTo[x]
    end
    path.unshift x
    path
  end

  # throw an IllegalArgumentException unless {@code 0 <= v < V}
  private
  def validateVertex(v)
    n = @marked.length
    raise ArgumentError.new("vertex #{v} is not between 0 and #{n-1}") if (v < 0 || v >= n)
  end

  # throw an IllegalArgumentException unless {@code 0 <= v < V}
  def validateVertices(vertices)
    raise ArgumentError.new("argument is null") if vertices == nil
    n = @marked.length;
    vertices.each do |v|
      raise ArgumentError.new("vertex #{v} is not between 0 and #{n-1}") if (v < 0 || v >= n)
    end
  end


  # /**
  #  * Unit tests the {@code BreadthFirstDirectedPaths} data type.
  #  *
  #  * @param args the command-line arguments
  #  */
  def self.main(args)
    inp = File.new(args[0].to_s)
    digraph =  Digraph.from_io(inp)
    # // StdOut.println(G);

    s = args[1].to_i
    bfs =  BreadthFirstDirectedPaths.new(digraph, s);

    (0...digraph.v).each do |v|
      if bfs.hasPathTo?(v)
        print "#{s} to #{v} (#{bfs.distTo(v)}):  "
        bfs.pathTo(v).each do |x|
          (x == s) ? (print "#{x}") : (print "->#{x}")
        end
        puts
      else
        puts "#{s} to #{v} (-):  not connected\n"
      end
    end
  end
end


# BreadthFirstDirectedPaths.main(ARGV)
# /******************************************************************************
#  *  Copyright 2002-2016, Robert Sedgewick and Kevin Wayne.
#  *
#  *  This file is part of algs4.jar, which accompanies the textbook
#  *
#  *      Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
#  *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
#  *      http://algs4.cs.princeton.edu
#  *
#  *
#  *  algs4.jar is free software: you can redistribute it and/or modify
#  *  it under the terms of the GNU General Public License as published by
#  *  the Free Software Foundation, either version 3 of the License, or
#  *  (at your option) any later version.
#  *
#  *  algs4.jar is distributed in the hope that it will be useful,
#  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  *  GNU General Public License for more details.
#  *
#  *  You should have received a copy of the GNU General Public License
#  *  along with algs4.jar.  If not, see http://www.gnu.org/licenses.
#  ******************************************************************************/
