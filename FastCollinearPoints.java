import java.util.Arrays;
import java.util.HashMap;
// import java.util.HashSet;
import java.util.Map;

// import edu.princeton.cs.algs4.StdOut;

public class FastCollinearPoints {
//  private int segments;
  private LineSegment[] segmentsList;
   public FastCollinearPoints(Point[] points) {     // finds all line segments containing 4 or more points
//     HashSet<Point> set = new HashSet<Point>();
     if (points == null)
       throw new NullPointerException("one of the points is null");
//     for (Point point: points) {
//       if (point == null)
//         throw new NullPointerException("one of the points is null");
     for (int j = 0; j < points.length; j++) {
       if (points[j] == null)
         throw new NullPointerException("one of the points is null");
       for (int k = j+1; k < points.length; k++)
         if (k != j && points[k].compareTo(points[j]) == 0)
           throw new IllegalArgumentException("one of the points is repeated");
//       boolean p = set.add(point);
//       if (!p)
//         throw new IllegalArgumentException("one of the points is repeated");
     }
//     set = new HashSet<Point>();
     
//     segments = 0;
     Map<String, LineSegment> lines = new HashMap<String, LineSegment>();
     
     int n = points.length;
     if (n < 4) {
       segmentsList = new LineSegment [0];
       return;
     }
       
     Point[] pointsC = new Point[n];
     System.arraycopy(points, 0, pointsC, 0, n);
//     Arrays.sort(pointsC);
//     for (Point each : pointsC) StdOut.print(" " + each);
//     StdOut.println();
     
     for (Point i : points) {
       Arrays.sort(pointsC);
       Arrays.sort(pointsC, i.slopeOrder());
//       for (Point each : pointsC) StdOut.print(" " + each);
//       StdOut.println();
       int t = 2;
       int counter = 2;
       int j = 1;
       Point low, high;
       double slope = pointsC[0].slopeTo(pointsC[j]);
       
       while (t < n) {
         if (slope == pointsC[0].slopeTo(pointsC[t])) {
           counter++;
         }
         else {
           if (counter >= 4) {
             if (i.compareTo(pointsC[j]) > 0) low = pointsC[j];
             else low = i;
             if (i.compareTo(pointsC[t-1]) >= 0) high = i;
             else high = pointsC[t-1];
             LineSegment line = new LineSegment(low, high);
             String key = low.toString()+high.toString();
             lines.put(key, line);
           }
           j = t;
           counter = 2;
           slope = pointsC[0].slopeTo(pointsC[j]);
         }
         t++;
       }
       if (counter >= 4) {
         if (i.compareTo(pointsC[j]) > 0) low = pointsC[j];
         else low = i;
         if (i.compareTo(pointsC[t-1]) >= 0) high = i;
         else high = pointsC[t-1];
         LineSegment line = new LineSegment(low, high);
         String key = low.toString()+high.toString();
         lines.put(key, line);
       }
//       for (Entry<String, LineSegment> entry: lines.entrySet()) {
//         System.out.println(entry.getKey() + " => " + entry.getValue());
//       }
     }
     segmentsList = lines.values().toArray(new LineSegment[0]);

//     System.out.println(numberOfSegments());

//     for (Entry<String, LineSegment> entry: lines.entrySet()) {
//       System.out.println(entry.getKey() + " => " + entry.getValue());
//     }
   }
   public           int numberOfSegments() {       // the number of line segments
     return segmentsList.length;
   }
   public LineSegment[] segments() {               // the line segments
     return segmentsList.clone();
   }
}