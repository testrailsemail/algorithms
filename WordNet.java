import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.DirectedCycle;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
// http://coursera.cs.princeton.edu/algs4/assignments/wordnet.html
// http://coursera.cs.princeton.edu/algs4/checklists/wordnet.html
public class WordNet {
  private final Map<String, Set<Integer>> nounToId;
  private final Map<Integer, String> idsToSynset;
  private final SAP sap;
   // constructor takes the name of the two input files
  public WordNet(String synsets, String hypernyms) {
    if (synsets == null || hypernyms == null) {
      throw new IllegalArgumentException("Two files are needed: synsets and hypernyms");
    }
    idsToSynset = new HashMap<>();
    nounToId = new HashMap<>();
    initSynSet(synsets);
    Digraph hnyms = initHypernyms(hypernyms);
    DirectedCycle cycle = new DirectedCycle(hnyms);
    if (cycle.hasCycle() || !isRootedDAG(hnyms)) {
      throw new java.lang.IllegalArgumentException("The input does not correspond to a rooted DAG!");
    }

    sap = new SAP(hnyms);

  }

  private void initSynSet(String synSets) {
    In synset = new In(synSets);

    while (!synset.isEmpty()) {
      String[] parts = synset.readLine().split(",");
      int id = Integer.parseInt(parts[0]);
      String syns = parts[1];
      idsToSynset.put(id, syns);
//    part[2] is a word definition

      for (String word : syns.split(" ")) {
        Set<Integer> ids = nounToId.get(word);
        if (ids == null) {
          ids = new HashSet<>();

        }
        ids.add(id);
        nounToId.put(word, ids);
      }

    }
  }

  private Digraph initHypernyms(String hypernyms) {
    Digraph g = new Digraph(idsToSynset.size());

    In nyms = new In(hypernyms);
    while (!nyms.isEmpty()) {
      String[] parts = nyms.readLine().split(",");
      int id = Integer.parseInt(parts[0]);
      for (int i = 1; i < parts.length; i++) {
        int part = Integer.parseInt(parts[i]);
        g.addEdge(id, part);
      }
    }
    StdOut.printf("vertices = %d, edges = %s\n", g.V(), g.E());

    return g;
  }

  private boolean isRootedDAG(Digraph g) {
    int roots = 0;
    for (int i = 0; i < g.V(); i++) {
      if (g.outdegree(i) == 0) {
        roots++;
        if (roots > 1)
          return false;
      }
    }
    return roots == 1;
  }

  // returns all WordNet nouns
  public Iterable<String> nouns() {
    return nounToId.keySet();
  }

   // is the word a WordNet noun?
  public boolean isNoun(String word) {
    if (word == null)
      throw new java.lang.IllegalArgumentException();
    if (word.getClass() != String.class)
      return false;
    return nounToId.containsKey(word);
  }

   // distance between nounA and nounB (defined below)
  public int distance(String nounA, String nounB) {
    if (!isNoun(nounA) || !isNoun(nounB)) {
      throw new java.lang.IllegalArgumentException("Words must be nouns");
    }
    Set<Integer> idsA = nounToId.get(nounA);
    Set<Integer> idsB = nounToId.get(nounB);

    return sap.length(idsA, idsB);
  }

   // a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
   // in a shortest ancestral path (defined below)
  public String sap(String nounA, String nounB) {
    if (!isNoun(nounA) || !isNoun(nounB)) {
      throw new java.lang.IllegalArgumentException("Words must be nouns");
    }
    Set<Integer> idsA = nounToId.get(nounA);
    Set<Integer> idsB = nounToId.get(nounB);
    int commonAncestor = sap.ancestor(idsA, idsB);

    return idsToSynset.get(commonAncestor);
  }

   // do unit testing of this class
  public static void main(String[] args) {
    WordNet wordNet = new WordNet(args[0], args[1]);

    while (!StdIn.isEmpty()) {
      String v = StdIn.readString();
      String w = StdIn.readString();
      if (!wordNet.isNoun(v)) {
          StdOut.println(v + " is not in the word net");
          continue;
      }
      if (!wordNet.isNoun(w)) {
          StdOut.println(w + " is not in the word net");
          continue;
      }

      int length = wordNet.distance(v, w);
      String ancestor = wordNet.sap(v, w);
      StdOut.printf("length = %d, ancestor = %s\n", length, ancestor);


    }
  }
}
